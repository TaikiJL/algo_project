/*

Adrien Deban
Nicolas Bordas
Taiki Hagiwara
Jeremy Wurtzel

Projet final
8INF870 - Algorithmique

Mesh de navigation

*/

#include <limits>
#include <unordered_set>

#include "NavMesh.h"

std::vector<glm::vec3> reconstruct_path(const std::vector<NavMesh::Noeud>& listeNoeuds, const std::vector<int>& came_from, int current);
bool isInPoly(const NavMesh& nav, const NavMesh::Polygon& pol, float x, float y);
float distance(NavMesh::Noeud start, NavMesh::Noeud goal);
std::vector<glm::vec3> Astar(const NavMesh& nav, const glm::vec3& startPos, const glm::vec3& goalPos);
