/*

Adrien Deban
Nicolas Bordas
Taiki Hagiwara
Jeremy Wurtzel

Projet final
8INF870 - Algorithmique

Mesh de navigation

*/

#include "AStar.h"

std::vector<glm::vec3> reconstruct_path(const std::vector<NavMesh::Noeud>& listeNoeuds, const std::vector<int>& came_from, int current)
{
	std::vector<glm::vec3> total_path;
	int indexActuel;
	total_path.push_back(glm::vec3(listeNoeuds[current].position));
	bool test = true;
	while (came_from[current] != -1)
	{
		current = came_from[current];
		total_path.push_back(glm::vec3(listeNoeuds[current].position));

// 		test = false;
// 		for (int node : came_from) {
// 			if (node == current){
// 				test = true;
// 				indexActuel = node;
// 				break;
// 			}
// 		}
// 		if (test){
// 			current = came_from[indexActuel];
// 			total_path.push_back(glm::vec3(listeNoeuds[current].position.x, listeNoeuds[current].position.y, 0));
// 		}
	}
	//total_path.pop_back();
	return (total_path);
}

bool isInPoly(const NavMesh& nav, const NavMesh::Polygon& pol, float x, float y)
{
	int i, j, c = 0;
	for (i = 0, j = pol.vertices.size() - 1; i < pol.vertices.size(); j = i++) {
		if ((((nav.Vertices()[pol.vertices[i]].y <= y) && (y < nav.Vertices()[pol.vertices[j]].y)) ||
			((nav.Vertices()[pol.vertices[j]].y <= y) && (y < nav.Vertices()[pol.vertices[i]].y))) &&
			(x < (nav.Vertices()[pol.vertices[j]].x - nav.Vertices()[pol.vertices[i]].x) * (y - nav.Vertices()[pol.vertices[i]].y) / (nav.Vertices()[pol.vertices[j]].y - nav.Vertices()[pol.vertices[i]].y) + nav.Vertices()[pol.vertices[i]].x))
			c = !c;
	}
	return c;
}

float distance(NavMesh::Noeud start, NavMesh::Noeud goal)
{
	float dx = goal.position.x - start.position.x;
	float dy = goal.position.y - start.position.y;
	return dx * dx + dy * dy;
	//return sqrt(pow(goal.position.x - start.position.x, 2) + pow(goal.position.y - start.position.y, 2));
}

std::vector<glm::vec3> Astar(const NavMesh& nav, const glm::vec3& startPos, const glm::vec3& goalPos)
{
	std::vector<NavMesh::Noeud> listeNoeuds = nav.Noeuds();
	std::unordered_set<int> closedSet;
	std::unordered_set<int> openSet;
	int startIndice = nav.Noeuds().size();
	int goalIndice = nav.Noeuds().size() + 1;
	std::vector<int> came_from(listeNoeuds.size() + 2, -1);

	std::vector<glm::vec3> resultat;
	std::vector<int> g_score(listeNoeuds.size() + 2);
	std::vector<int> f_score(listeNoeuds.size() + 2);
	NavMesh::Noeud start;
	NavMesh::Noeud goal;
	int current;
	float tentative_g_score;

	start.position = startPos;
	goal.position = goalPos;
	bool pointPlace = false;
	float min;

	int startPolyIndex, goalPolyIndex;
	startPolyIndex = goalPolyIndex = -1;

	// initialisation Start
	for (int i = 0; i < nav.Polygons().size() && !pointPlace; i++){
		pointPlace = isInPoly(nav, nav.Polygons()[i], start.position.x, start.position.y);
		if (pointPlace){
			for (int j = 0; j < nav.Polygons()[i].noeuds.size(); j++){
				listeNoeuds[nav.Polygons()[i].noeuds[j]].noeudsAdj.push_back(startIndice);
				start.noeudsAdj.push_back(nav.Polygons()[i].noeuds[j]);
			}
			startPolyIndex = i;
		}
	}

	pointPlace = false;

	// initialisation Goal
	for (int i = 0; i < nav.Polygons().size() && !pointPlace; i++){
		pointPlace = isInPoly(nav, nav.Polygons()[i], goal.position.x, goal.position.y);
		if (pointPlace){
			for (int j = 0; j < nav.Polygons()[i].noeuds.size(); j++){
				listeNoeuds[nav.Polygons()[i].noeuds[j]].noeudsAdj.push_back(goalIndice);
				goal.noeudsAdj.push_back(nav.Polygons()[i].noeuds[j]);
			}
			goalPolyIndex = i;
		}
	}

	// Si les deux points sont dans le m�me polygone, pas d'A* � faire
	if (startPolyIndex == goalPolyIndex)
	{
		return std::vector<glm::vec3>(1, goalPos);
	}
	// Si un des points n'est dans aucun polygone, on reste au m�me endroit.
	else if (startPolyIndex == -1 || goalPolyIndex == -1)
	{
		return std::vector<glm::vec3>();
	}

	listeNoeuds.push_back(start);
	listeNoeuds.push_back(goal);
	
	g_score[startIndice] = 0;
	f_score[startIndice] = g_score[startIndice] + distance(start, goal);
	openSet.insert(startIndice);

	while (openSet.size() != 0)
	{
		min = std::numeric_limits<float>::max();
		for (int elem : openSet)
		{
			if (f_score[elem] < min)
			{
				min = f_score[elem];
				current = elem;
			}
		}

		if (current == goalIndice)
		{
			return reconstruct_path(listeNoeuds, came_from, goalIndice);
		}
		else
		{
			openSet.erase(current);
			closedSet.insert(current);
			
			for (int node : listeNoeuds[current].noeudsAdj)
			{
				if (closedSet.find(node) != closedSet.end())
					continue;
// 				test = true;
// 				for (int point : closedSet){
// 					if (node == point)
// 					{
// 						test = false;
// 						break;
// 					}
// 				}
// 				if (!test)
// 				{
// 					continue;
// 				}
				tentative_g_score = g_score[current] + distance(listeNoeuds[current], listeNoeuds[node]);

// 				test = true;
// 				for (int point : openSet)
// 				{
// 					if (point == node)
// 					{
// 						test = false;
// 						break;
// 					}
// 				}

				bool voisinVisite = openSet.find(node) == openSet.end();
				if (voisinVisite || tentative_g_score < g_score[node])
				{
					came_from[node] = current;
					g_score[node] = tentative_g_score;
					f_score[node] = g_score[node] + distance(listeNoeuds[node], goal);
					if (voisinVisite)
						openSet.insert(node);
				}
			}
		}
	}

	return std::vector<glm::vec3>();
}
