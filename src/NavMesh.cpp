/*

Adrien Deban
Nicolas Bordas
Taiki Hagiwara
Jeremy Wurtzel

Projet final
8INF870 - Algorithmique

Mesh de navigation

*/

#include "NavMesh.h"

bool CheckPointsProximite(const glm::vec3& a, const glm::vec3& b, float distanceMax = 0.01f)
{
	float dx = std::fabsf(a.x - b.x);
	if (dx > distanceMax) return false;
	float dy = std::fabsf(a.y - b.y);
	if (dy > distanceMax) return false;
	return true;
}

std::shared_ptr<Object> NavMesh::CreationNavMesh(const ObjectData& donnees)
{
	auto navMesh = std::make_shared<NavMesh>();
	auto ptr = std::dynamic_pointer_cast<Object, NavMesh>(navMesh);
	
	navMesh->AddComponent<GraphicsComponent>();
	auto graphics = navMesh->GetComponent<GraphicsComponent>();
	Scene::Current().AddComponentToCollection(graphics);

	const auto& couleur = donnees.GetStruct("color");
	graphics->Color(glm::vec4(couleur.GetFloatVar("r"),
		couleur.GetFloatVar("g"),
		couleur.GetFloatVar("b"),
		couleur.GetFloatVar("a")));

	graphics->SetMesh(&(navMesh->_navMesh));

	const std::string& mesh = donnees.GetStringVar("mesh");
	//navMesh->GenererMesh(mesh.c_str());

	return ptr;
}

void NavMesh::Initialize()
{
	auto obstacles = Scene::Current().GetObjectsOfType<Obstacle>();
	auto terrain = Scene::Current().GetObjectsOfType<Terrain>()[0];

	auto navMeshGraphics = this->GetComponent<GraphicsComponent>();
	navMeshGraphics->IsEnabled(false);

	Input::Singleton().RegisterKeyCommand(SDLK_t, SDL_KEYDOWN, [=](){
		for (auto obstacle : obstacles)
		{
			auto graphics = obstacle->GetComponent<GraphicsComponent>();
			graphics->IsEnabled(!(graphics->IsEnabled()));
		}
		auto terrainGraphics = terrain->GetComponent<GraphicsComponent>();
		terrainGraphics->IsEnabled(!(terrainGraphics->IsEnabled()));
		navMeshGraphics->IsEnabled(!(navMeshGraphics->IsEnabled()));
	});

	std::vector<const Mesh*> obstacleMeshes;
	obstacleMeshes.reserve(obstacles.size());
	for (auto obstacle : obstacles)
	{
		auto graphics = obstacle->GetComponent<GraphicsComponent>();
		obstacleMeshes.push_back(graphics->GetMesh());
	}
	auto terrainMesh = terrain->GetComponent<GraphicsComponent>()->GetMesh();
	
	GenererMesh(obstacleMeshes, terrainMesh);
}

void NavMesh::GenererMesh(const std::vector<const Mesh*>& obstacles, const Mesh* terrain)
{
	struct Edge
	{
		Edge(int a, int b) : a(a <= b ? a : b), b(b >= a ? b : a) {}
		int a, b;
	};

	std::vector<std::vector<Point>> matObstacle;

	for (const auto& obstacle : obstacles)
	{
		std::vector<std::vector<int>> areteDejaTrouvee(obstacle->Vertices().size(),
			std::vector<int>(obstacle->Vertices().size(), 0));
		std::vector<Edge> aretesUniques;
		matObstacle.push_back(std::vector<Point>());
		const auto& obstacleIndices = obstacle->Indices();
		for (int i = 0; i < obstacleIndices.size(); ++i)
		{
			// Si c'est le dernier point du triangle...
			Edge edge = (i + 1) % 3 == 0 ? Edge(obstacleIndices[i], obstacleIndices[i - 2]) :
				Edge(obstacleIndices[i], obstacleIndices[i + 1]);

			areteDejaTrouvee[edge.a][edge.b] += 1;
		}
		for (int i = 0; i < obstacle->Vertices().size(); ++i)
		{
			for (int j = 0; j < obstacle->Vertices().size(); ++j)
			{
				if (areteDejaTrouvee[i][j] == 1) aretesUniques.push_back(Edge(i, j));
			}
		}

		unsigned int dernierIndex = aretesUniques[0].a;
		{
			auto vertex = obstacle->Vertices()[dernierIndex];
			matObstacle.back().push_back(Point(vertex.x, vertex.y));
		}
		while (aretesUniques.size() > 1)
		{
			for (int i = 1; i < aretesUniques.size(); ++i)
			{
				bool paireTrouvee = false;
				if (dernierIndex == aretesUniques[i].a)
				{
					dernierIndex = aretesUniques[i].b;
					paireTrouvee = true;
				}
				else if (dernierIndex == aretesUniques[i].b)
				{
					dernierIndex = aretesUniques[i].a;
					paireTrouvee = true;
				}

				if (paireTrouvee)
				{
					auto vertex = obstacle->Vertices()[dernierIndex];
					matObstacle.back().push_back(Point(vertex.x, vertex.y));
					aretesUniques[0] = aretesUniques[i];
					aretesUniques.erase(aretesUniques.begin() + i);
					break;
				}
			}
		}
	}

	//////////////////////////////////////////////////////////////////////

	std::vector<glm::vec3> vertices;
	std::vector<unsigned int> indices;

	int offset = 0;
	for (const auto& obstacle : matObstacle)
	{
		vertices.push_back(glm::vec3(obstacle[0].X, obstacle[0].Y, 0.f));
		for (int i = 0; i < obstacle.size() - 1; ++i)
		{
			indices.push_back(offset + i);
			vertices.push_back(glm::vec3(obstacle[i + 1].X, obstacle[i + 1].Y, 0.f));
			indices.push_back(offset + i + 1);
		}
		indices.push_back(offset + obstacle.size() - 1);
		indices.push_back(offset);
		offset += obstacle.size();
	}

	std::cout << "DEBUT" << std::endl;
	std::vector<Polygone> listePOLY = FaireNavMesh(matObstacle, terrain);
	std::cout << "FIN" << std::endl;

	std::vector<Polygon> triangles;
	std::vector<std::vector<Edge>> aretesPoly;

	for (int i = 0; i < listePOLY.size(); i++)
	{
		bool triangleDsPoly = false;
		for (auto obstacle : matObstacle)
		{
			Polygone obstaclePoly;
			obstaclePoly.Vertices = std::vector<Point>(obstacle);
			int nPointDsPoly = 0;
			for (auto sommet : listePOLY[i].Vertices)
			{
				if (EstDansPolygone(obstaclePoly, sommet.X, sommet.Y)) ++nPointDsPoly;
			}
			if (nPointDsPoly >= 3) triangleDsPoly;
			if (triangleDsPoly) break;
		}
		if (triangleDsPoly) continue;

		triangles.push_back(Polygon());
		for (int j = 0; j < listePOLY[i].Vertices.size(); j++) // normalement = 3
		{
			int indexVertex = -1;
			glm::vec3 point(listePOLY[i].Vertices[j].X, listePOLY[i].Vertices[j].Y, 0.f);
			for (int k = 0; k < vertices.size(); ++k)
			{
				if (CheckPointsProximite(vertices[k], point))
				{
					indexVertex = k;
					break;
				}
			}

			if (indexVertex < 0)
			{
				vertices.push_back(point);
				triangles.back().vertices.push_back(vertices.size() - 1);
			}
			else
			{
				triangles.back().vertices.push_back(indexVertex);
			}
		}

		aretesPoly.push_back(std::vector<Edge>(3, Edge(0, 0)));
		const Edge& edge1 = aretesPoly.back()[0] = Edge(triangles.back().vertices[0], triangles.back().vertices[1]);
		const Edge& edge2 = aretesPoly.back()[1] = Edge(triangles.back().vertices[1], triangles.back().vertices[2]);
		const Edge& edge3 = aretesPoly.back()[2] = Edge(triangles.back().vertices[2], triangles.back().vertices[0]);
		indices.push_back(edge1.a); indices.push_back(edge1.b);
		indices.push_back(edge2.a); indices.push_back(edge2.b);
		indices.push_back(edge3.a); indices.push_back(edge3.b);
	}

	/////// Creation noeuds milieu

	std::vector<std::vector<std::vector<unsigned int>>> edgeMatrice(vertices.size(),
		std::vector<std::vector<unsigned int>>(vertices.size()));

	for (int i = 0; i < aretesPoly.size(); ++i)
	{
		for (auto edge : aretesPoly[i])
			edgeMatrice[edge.a][edge.b].push_back(i);
	}

	std::vector<Noeud> noeuds;
	for (int i = 0; i < edgeMatrice.size(); ++i)
	{
		for (int j = 0; j < edgeMatrice[i].size(); ++j)
		{
			if (edgeMatrice[i][j].size() > 1)
			{
				Noeud noeud;
				glm::vec3 a = vertices[i];
				glm::vec3 b = vertices[j];
				noeud.position = (a + b) * 0.5f;
				noeuds.push_back(noeud);
				for (auto poly : edgeMatrice[i][j])
					triangles[poly].noeuds.push_back(noeuds.size() - 1);
			}
		}
	}

	for (const auto& triangle : triangles)
	{
		for (int i = 0; i < triangle.noeuds.size(); ++i)
		{
			for (int j = 0; j < triangle.noeuds.size(); ++j)
				if (i != j) noeuds[triangle.noeuds[i]].noeudsAdj.push_back(triangle.noeuds[j]);
		}
	}

	//Afficher liens noeuds
	//int verticesSize = vertices.size();
	//int count = 0;
	//for (const auto& noeud : noeuds)
	//{
	//	vertices.push_back(noeud.position);
	//	for (auto noeudAdj : noeud.noeudsAdj)
	//	{
	//		indices.push_back(count + verticesSize);
	//		indices.push_back(noeudAdj + verticesSize);
	//	}
	//	++count;
	//}
	//Afficher liens noeuds fin

	_noeuds = std::move(noeuds);
	_polygons = std::move(triangles);

	/////////// fin creation noeuds milieu

	std::cout << listePOLY.size() << std::endl;
	_navMesh.LoadLineVertices(std::move(vertices), std::move(indices));
	_vertices = &(_navMesh.Vertices());
}

void NavMesh::GenererMesh(const char* chemin, float scale)
{
	std::ifstream fichier;
	fichier.open(chemin, std::ifstream::in);

	if (!fichier)
	{
		std::cerr << "Erreur : ouverture du fichier \"" << chemin << "\" impossible\n";
	}
	assert(fichier);

	std::vector<glm::vec3> vertices;
	std::vector<unsigned int> indices;
	std::vector<Polygon> polygons;

	char ligne[512];
	
	while (fichier.getline(ligne, sizeof(ligne)))
	{
		std::stringstream fluxLigne(ligne);
		
		char lettre[2];
		fluxLigne >> lettre;
		
		if (strcmp(lettre, "v") == 0)
		{
			while (!fluxLigne.eof())
			{
				float x, y , z;
				fluxLigne >> x >> y >> z;
				vertices.push_back(glm::vec3(x, y, z) * scale);
			}
		}
		else if (strcmp(lettre, "f") == 0)
		{
			//aretesPoly.push_back(std::vector<Edge>());
			polygons.push_back(Polygon());
			int premierIndex;
			fluxLigne >> premierIndex;
			indices.push_back(--premierIndex);
			polygons.back().vertices.push_back(premierIndex);
			while (!fluxLigne.eof())
			{
				int index;
				if (fluxLigne >> index)
				{
					indices.push_back(--index);
					polygons.back().vertices.push_back(index);
					//aretesPoly.back().push_back(Edge(indices[indices.size() - 2], indices.back()));
					indices.push_back(index);
				}
			}
			//aretesPoly.back().push_back(Edge(indices.back(), premierIndex));
			indices.push_back(premierIndex);
		}
		else
		{
			continue;
		}
	}
	
	struct Edge
	{
		Edge(int a, int b) : a(a <= b ? a : b), b(b >= a ? b : a) {}
		int a, b;
	};
	std::vector<std::vector<Edge>> aretesPoly;
	for (const auto& poly : polygons)
	{
		aretesPoly.push_back(std::vector<Edge>());
		for (int i = 0; i < poly.vertices.size() - 1; ++i)
		{
			aretesPoly.back().push_back(Edge(poly.vertices[i], poly.vertices[i + 1]));
		}
		aretesPoly.back().push_back(Edge(poly.vertices[poly.vertices.size() - 1], poly.vertices[0]));
	}

	std::vector<std::vector<std::vector<unsigned int>>> edgeMatrice(vertices.size(),
		std::vector<std::vector<unsigned int>>(vertices.size()));

	for (int i = 0; i < aretesPoly.size(); ++i)
	{
		for (auto edge : aretesPoly[i])
			edgeMatrice[edge.a][edge.b].push_back(i);
	}

	std::vector<Noeud> noeuds;
	for (int i = 0; i < edgeMatrice.size(); ++i)
	{
		for (int j = 0; j < edgeMatrice[i].size(); ++j)
		{
			if (edgeMatrice[i][j].size() > 1)
			{
				Noeud noeud;
				glm::vec3 a = vertices[i];
				glm::vec3 b = vertices[j];
				noeud.position = (a + b) * 0.5f;
				noeuds.push_back(noeud);
				for (auto poly : edgeMatrice[i][j])
					polygons[poly].noeuds.push_back(noeuds.size() - 1);
			}
		}
	}

	for (const auto& poly : polygons)
	{
		for (int i = 0; i < poly.noeuds.size(); ++i)
		{
			for (int j = 0; j < poly.noeuds.size(); ++j)
				if (i != j) noeuds[poly.noeuds[i]].noeudsAdj.push_back(poly.noeuds[j]);
		}
	}

	_navMesh.LoadLineVertices(std::move(vertices), std::move(indices));

	_vertices = &(_navMesh.Vertices());
	_noeuds = std::move(noeuds);
	_polygons = std::move(polygons);

	fichier.close();

}



/* Fonction pour la recup�ration du contour des obstacles */

void NavMesh::AfficherPointObstacles(std::vector<Point> listeObstacles){
	for (int i = 0; i < listeObstacles.size(); i++)
	{
		std::cout << listeObstacles[i].X << "	";
		std::cout << listeObstacles[i].Y << "	";
		std::cout << std::endl;
	}
}

void NavMesh::AfficherUnPoint(Point unPoint){
	std::cout << "COORDONNEES POINT : " << unPoint.X << "	" << unPoint.Y << std::endl;
}

void NavMesh::AfficherMatriceObstacles(std::vector < std::vector < Point> > matPoint){
	for (int i = 0; i < matPoint.size(); i++)
	{
		std::cout << "DEBUT LISTE POINT" << std::endl;
		for (int j = 0; j < matPoint[i].size(); j++)
		{
			std::cout << matPoint[i][j].X << "	";
			std::cout << matPoint[i][j].Y << "	";
			std::cout << std::endl;
		}
		std::cout << "FIN LISTE POINT" << std::endl;
	}
}

void NavMesh::AfficherSegment(Point A, Point B, Point C, Point D){
	std::vector<glm::vec3> vertices;
	std::vector<unsigned int> indices;
	vertices.push_back(glm::vec3(A.X, A.Y, 0.f));
	indices.push_back(0);
	vertices.push_back(glm::vec3(B.X, B.Y, 0.f));
	indices.push_back(1);
	vertices.push_back(glm::vec3(C.X, C.Y, 0.f));
	indices.push_back(2);
	vertices.push_back(glm::vec3(D.X, D.Y, 0.f));
	indices.push_back(3);
	_navMesh.LoadLineVertices(std::move(vertices), std::move(indices));
}

std::vector<std::vector<Point>> NavMesh::CopierPointObstacles(const std::vector<const Mesh*>& obstacles){
	std::vector<std::vector<Point>> matricePoint(obstacles.size());
	Point unPoint;
	int i = 0;
	for (auto obstacle : obstacles)
	{
		//matricePoint.back().push_back(std::vector<Point>());
		for (int j = 0; j < obstacle->Indices().size(); j++){
			unPoint.X = obstacle->Vertices()[obstacle->Indices()[j]].x;
			unPoint.Y = obstacle->Vertices()[obstacle->Indices()[j]].y;
			matricePoint[i].push_back(unPoint);
		}
		i++;
	}
	return matricePoint;
}

/* Fonction qui permet de recuperer tous les noeuds du contour d'un obstacle */
std::vector < std::vector < Point> > NavMesh::TrouverObstacles(const std::vector<const Mesh*>& obstacles, const Mesh* terrain){
	std::vector < Point>   listePointRestant, lstOrigine;
	std::vector < std::vector < Point> > matriceOrigine, matriceObstacles(obstacles.size());
	matriceOrigine = CopierPointObstacles(obstacles);
	Point lePoint, leDeuxiemePoint, leTemp;
	
	for (int i = 0; i < matriceOrigine.size(); i++)
	{
		if (lstOrigine.size()>0){
			lstOrigine.clear();
		}
	
		for (int j = 0; j < matriceOrigine[i].size(); j++)
		{
			lstOrigine.push_back(matriceOrigine[i][j]);
		}
		//AfficherPointObstacles(lstOrigine);
		//std::cout << "Fin point" << std::endl;
		listePointRestant = lstOrigine;
		lePoint = PlusGrandX(lstOrigine);														//**prendre le noued avec la plus grande valeur de x
		//std::cout << "point 1" << std::endl;
		//AfficherUnPoint(lePoint);
		matriceObstacles[i].push_back(lePoint);														//**le stocker dans pointObstacle
		//std::cout << "OK" << std::endl;
		listePointRestant = TrouverPolygoneDePoint(lstOrigine, matriceObstacles[i].back());			//**r�cup�re les noeuds adjacents au noeud lePoint
		leDeuxiemePoint = PlusGrandX(listePointRestant);
		//std::cout << "point 2" << std::endl;
		//AfficherUnPoint(leDeuxiemePoint);
		leTemp = trouverSuivantNoeudObstacle(lePoint, leDeuxiemePoint, listePointRestant);		//**r�cup�re le noeud suivant de l'obstacle
		//std::cout << "point suivant" << std::endl;
		//AfficherUnPoint(leTemp);
		matriceObstacles[i].push_back(leTemp);
		int compt = 0;
		do{
			leTemp = trouverSuivantNoeudObstacle(matriceObstacles[i].back(), matriceObstacles[i][matriceObstacles[i].size() - 2], listePointRestant);
			//std::cout << "point suivant" << std::endl;
			//AfficherUnPoint(leTemp);
			matriceObstacles[i].push_back(leTemp);
				compt++;
		//} while (matriceObstacles[i][0].X != matriceObstacles[i][matriceObstacles[i].size() - 1].X && matriceObstacles[i][0].Y != matriceObstacles[i][matriceObstacles[i].size() - 1].Y);
		} while (compt<10);
		std::cout << "MATRICE" << std::endl;
		AfficherMatriceObstacles(matriceObstacles);
		std::cout << "FIN MATRICE" << std::endl;
	}
	return matriceObstacles;
}

/* recupere le noeud avec la coordonnees x la plus grande */
Point NavMesh::PlusGrandX(std::vector<Point> listePoint){
	Point p;
	p.X = -10;
	for (int i = 0; i < listePoint.size(); i++)
	{
		if (listePoint[i].X > p.X){
			p.X = listePoint[i].X;
			p.Y = listePoint[i].Y;
		}
	}
	return p;
}

/* Fonction pour r�cuperer les noeuds adjacents au noeud pass�  en parametre de la fonction */
std::vector<Point> NavMesh::TrouverPolygoneDePoint(std::vector<Point> listePoint, Point unPoint){

	std::vector<Point> lstResultat;
	lstResultat.push_back(unPoint);

	for (int i = 0; i < listePoint.size(); i++)
	{
		if (listePoint[i].X == unPoint.X && listePoint[i].Y == unPoint.Y)
		{
			bool estPresent = false;
			int k = (i / 3) * 3;
			int l = ((i / 3) * 3) + 1;
			int m = ((i / 3) * 3) + 2;
			for (int j = 0; j < lstResultat.size(); j++)
			{
				if (listePoint[k].X == lstResultat[j].X && listePoint[k].Y == lstResultat[j].Y)
				{
					estPresent = true;
				}
			}
			if (estPresent == false)
			{
				lstResultat.push_back(listePoint[k]);
			}
			estPresent = false;
			for (int j = 0; j < lstResultat.size(); j++)
			{
				if (listePoint[l].X == lstResultat[j].X && listePoint[l].Y == lstResultat[j].Y)
				{
					estPresent = true;
				}
			}
			if (estPresent == false)
			{
				lstResultat.push_back(listePoint[l]);
			}
			estPresent = false;
			for (int j = 0; j < lstResultat.size(); j++)
			{
				if (listePoint[m].X == lstResultat[j].X && listePoint[m].Y == lstResultat[j].Y)
				{
					estPresent = true;
				}
			}
			if (estPresent == false)
			{
				lstResultat.push_back(listePoint[m]);
			}
		}
	}
	lstResultat.erase(lstResultat.begin());
	//std::cout << "points lier a ce point" << std::endl;
	//AfficherPointObstacles(lstResultat);
	//std::cout << "Fin de la liste" << std::endl;
	return lstResultat;

}

/* Fonction qui va retourner le prochain noeud du contour de l'ostacle */
Point NavMesh::trouverSuivantNoeudObstacle(Point unPoint, Point deuxPoint, std::vector<Point> listePoint){
	Point leMeilleurPoint;
	leMeilleurPoint.X = -10;
	leMeilleurPoint.Y = -10;
	float X1, Y1, X2, Y2, angle, u, v, temp;
	X1 = deuxPoint.X - unPoint.X;
	Y1 = deuxPoint.Y - unPoint.Y;
	u = sqrt(X1*X1 + Y1*Y1);

	for (int i = 0; i < listePoint.size(); i++)
	{
		if (listePoint[i].X == unPoint.X&&listePoint[i].Y == unPoint.Y || listePoint[i].X == deuxPoint.X&&listePoint[i].Y == deuxPoint.Y)
		{

		}
		else{
			angle = 10000;
			X2 = listePoint[i].X - unPoint.X;
			Y2 = listePoint[i].Y - unPoint.Y;
			v = sqrt(X2*X2 + Y2*Y2);
			temp = acos((X1*Y1 + X2*Y2) / (u*v));						//** angle calcul�
			std::cout << "ANGLE" << temp << std::endl;
			if (temp<angle)
			{
				angle = temp;
				leMeilleurPoint.X = listePoint[i].X;
				leMeilleurPoint.Y = listePoint[i].Y;
			}
		}

	}

	std::cout << "ON PREND CE POINT" << leMeilleurPoint.X << "	" << leMeilleurPoint.Y << std::endl;
	return leMeilleurPoint;

}























/* Fonction pour la triangulation */

/* fonction pour r�cuperer les polygones du terrain initial */

std::vector<Polygone> NavMesh::FaireNavMesh(std::vector < std::vector < Point> > matPointObstacles, const Mesh* terrain){
	std::vector<Polygone> listePolygones, listeNouveauPolygones, listePolyTemp1, listePolyTemp2;
	std::vector<Point> listePointTerrain;
	Polygone unPolygone;
	listePointTerrain = TrouverPointTerrain(terrain);
	//AfficherPointObstacles(listePointTerrain);
	for (int i = 0; i < listePointTerrain.size(); i=i+3)
	{
		unPolygone.Vertices.push_back(listePointTerrain[i]);
		unPolygone.Vertices.push_back(listePointTerrain[i+1]);
		unPolygone.Vertices.push_back(listePointTerrain[i+2]);
		listePolygones.push_back(unPolygone);
		unPolygone.Vertices.clear();
	}
	listeNouveauPolygones = listePolygones;
	for (int i = 0; i < matPointObstacles.size(); i++)
	{
		for (int j = 0; j < listePolygones.size(); j++)
		{	
			for (int k = 0; k < matPointObstacles[i].size(); k++)
			{	
				if (IntersectionSegmentPolygone(listeNouveauPolygones[j], matPointObstacles[i][k], matPointObstacles[i][(k + 1) % (matPointObstacles[i].size())]))
				{
					Segment s;
					s.A = matPointObstacles[i][k];
					s.B = matPointObstacles[i][(k + 1) % (matPointObstacles[i].size())];
					listeNouveauPolygones = Trianguler(listePolygones, listeNouveauPolygones,s, j);
				}
			}
		}
	}
	return listeNouveauPolygones;
}




std::vector<Polygone> NavMesh::Trianguler(std::vector<Polygone> listePolygonesOrigine, std::vector<Polygone> listeNouveauPolygones, Segment segment, int p){
	std::vector<Point> lstTemp;
	std::vector<Polygone> listePolygonesTemporaire;
		for (int j = 0; j < listePolygonesOrigine[p].Vertices.size(); j++)
		//for (int j = 0; j < listeNouveauPolygones[p].Vertices.size(); j++)
		{
			//std::cout << "OK10" << std::endl;
			//AfficherUnPoint(segment.A);
			//AfficherUnPoint(segment.B);
			//AfficherUnPoint(listePolygonesOrigine[p].Vertices[j]);
			//AfficherUnPoint(listePolygonesOrigine[p].Vertices[(j + 1) % (listePolygonesOrigine[p].Vertices.size())]);

			if (PointIntersectionSegmentDroite(segment.A.X, segment.A.Y, segment.B.X, segment.B.Y, listePolygonesOrigine[p].Vertices[j].X, listePolygonesOrigine[p].Vertices[j].Y, listePolygonesOrigine[p].Vertices[(j + 1) % (listePolygonesOrigine[p].Vertices.size())].X, listePolygonesOrigine[p].Vertices[(j + 1) % (listePolygonesOrigine[p].Vertices.size())].Y))
			//if (PointIntersectionSegmentDroite(segment.A.X, segment.A.Y, segment.B.X, segment.B.Y, listeNouveauPolygones[p].Vertices[j].X, listeNouveauPolygones[p].Vertices[j].Y, listeNouveauPolygones[p].Vertices[(j + 1) % (listeNouveauPolygones[p].Vertices.size())].X, listeNouveauPolygones[p].Vertices[(j + 1) % (listeNouveauPolygones[p].Vertices.size())].Y))
			{
				
				lstTemp.push_back(RecupPointIntersectionSegmentDroite(segment.A.X, segment.A.Y, segment.B.X, segment.B.Y, listePolygonesOrigine[p].Vertices[j].X, listePolygonesOrigine[p].Vertices[j].Y, listePolygonesOrigine[p].Vertices[(j + 1) % (listePolygonesOrigine[p].Vertices.size())].X, listePolygonesOrigine[p].Vertices[(j + 1) % (listePolygonesOrigine[p].Vertices.size())].Y));
				//lstTemp.push_back(RecupPointIntersectionSegmentDroite(segment.A.X, segment.A.Y, segment.B.X, segment.B.Y, listeNouveauPolygones[p].Vertices[j].X, listeNouveauPolygones[p].Vertices[j].Y, listeNouveauPolygones[p].Vertices[(j + 1) % (listeNouveauPolygones[p].Vertices.size())].X, listeNouveauPolygones[p].Vertices[(j + 1) % (listeNouveauPolygones[p].Vertices.size())].Y));
				//std::cout << "COOL" << std::endl;
			}
		}
		std::cout << "d" << std::endl;
		for (int t = 0; t < lstTemp.size(); t++)
		{
			AfficherUnPoint(lstTemp[t]);
		}
		std::cout << "f" << std::endl;
		//for (int i = 0; i < lstTemp.size(); i++)
		//{
			bool pointOK = false;
			for (int j = 0; j < listePolygonesOrigine[p].Vertices.size() && pointOK == false; j++)
			{
				Segment s;
				s.A = listePolygonesOrigine[p].Vertices[j];
				s.B = listePolygonesOrigine[p].Vertices[(j + 1) % (listePolygonesOrigine[p].Vertices.size())];
				if (EstEntreSegment(s.A, s.B, lstTemp[0]))
				{
					listeNouveauPolygones = CreerDeuxTriangles(p, lstTemp[0], listeNouveauPolygones,s);
						bool estOK = false;
						for (int k = 0; k < listeNouveauPolygones[p].Vertices.size(); k++)
						{
							Segment s;
							s.A = listeNouveauPolygones[p].Vertices[k];
							s.B = listeNouveauPolygones[p].Vertices[(k + 1) % (listeNouveauPolygones[p].Vertices.size())];
							if (EstEntreSegment(s.A, s.B, lstTemp[1]))
							{
								listeNouveauPolygones = CreerDeuxTriangles(p, lstTemp[1], listeNouveauPolygones, s);
								std::cout << "OUAIS1" << std::endl;
								estOK = true;
							}
						}
						if (estOK == false)
						{
							for (int k = 0; k < listeNouveauPolygones[p].Vertices.size(); k++)
							{
								Segment s;
								s.A = listeNouveauPolygones[listeNouveauPolygones.size() - 1].Vertices[k];
								s.B = listeNouveauPolygones[listeNouveauPolygones.size() - 1].Vertices[(k + 1) % (listeNouveauPolygones[p].Vertices.size())];
								if (EstEntreSegment(s.A, s.B, lstTemp[1]))
								{
									listeNouveauPolygones = CreerDeuxTriangles(p, lstTemp[1], listeNouveauPolygones, s);
									std::cout << "OUAIS2" << std::endl;
								}
							}
						}
						pointOK = true;
					}
			}
		//}
		

	return listeNouveauPolygones;

}





std::vector < Point> NavMesh::TrouverPointTerrain(const Mesh* terrain){
	std::vector<Point> listePoint;
	Point unPoint;
	for (int j = 0; j < terrain->Indices().size(); j++){
		unPoint.X = terrain->Vertices()[terrain->Indices()[j]].x;
		unPoint.Y = terrain->Vertices()[terrain->Indices()[j]].y;
		listePoint.push_back(unPoint);
	}
	return listePoint;
}


bool NavMesh::Intersection(float x1, float y1, float x2, float y2, float x3, float y3, float x4, float y4)
{

	float d0, d1, d2, d3;
	d0 = x2 - x1;     d1 = y2 - y1;
	d2 = x4 - x3;     d3 = y4 - y3;

	float s, t;
	s = (-d1 * (x1 - x3) + d0 * (y1 - y3)) / (-d2 * d1 + d0 * d3);
	t = (d2 * (y1 - y3) - d3 * (x1 - x3)) / (-d2 * d1 + d0 * d3);

	if (s >= 0 && s <= 1 && t >= 0 && t <= 1)
	{
		// collision avec un obstacle
		return true;
	}

	return false; // pas de collision


}

Point NavMesh::RecupPointIntersectionSegment(float x1, float y1, float x2, float y2, float x3, float y3, float x4, float y4){
	Point Resultat;
	float s02_x, s02_y, s10_x, s10_y, s32_x, s32_y, s, te, denom, t;
	s10_x = x2 - x1;
	s10_y = y2 - y1;
	s32_x = x4 - x3;
	s32_y = y4 - y3;

	denom = s10_x * s32_y - s32_x * s10_y;
	s02_x = x1 - x3;
	s02_y = y1 - y3;
	s = s10_x * s02_y - s10_y * s02_x;
	te = s32_x * s02_y - s32_y * s02_x;	
	t = te / denom;
	Resultat.X = x1 + (t * s10_x);
	Resultat.Y = y1 + (t * s10_y);
	std::cout << std::endl;	
	return Resultat;
}

bool NavMesh::EstDansPolygone(Polygone polygone, float x, float y) {
	int i, j;
	bool c = false;

	for (i = 0, j = polygone.Vertices.size() - 1; i < polygone.Vertices.size(); j = i++) {
		if (((polygone.Vertices[i].Y >= y) != (polygone.Vertices[j].Y >= y)) && (x <= (polygone.Vertices[j].X - polygone.Vertices[i].X) * (y - polygone.Vertices[i].Y) / (polygone.Vertices[j].Y - polygone.Vertices[i].Y) + x))
			c = !c;
	}

	return c;
}

bool NavMesh::IntersectionSegmentPolygone(Polygone polygone, Point A, Point B){
	bool c = false;
	bool temp;
	for (int i = 0; i < polygone.Vertices.size() - 1; i++) {
		temp = Intersection(polygone.Vertices[i].X, polygone.Vertices[i].Y, polygone.Vertices[i + 1].X, polygone.Vertices[i+1].Y, A.X, A.Y, B.X, B.Y);
		if (temp == true){
			c = true;
		}
	}
	temp = Intersection(polygone.Vertices[polygone.Vertices.size() - 1].X, polygone.Vertices[polygone.Vertices.size() - 1].Y, polygone.Vertices[0].X, polygone.Vertices[0].Y, A.X, A.Y, B.X, B.Y);
	if (temp == true){
		c = true;
	}
	return c;
}

bool NavMesh::PointIntersectionSegmentDroite(float dx1, float dy1, float dx2, float dy2, float sx1, float sy1, float sx2, float sy2){
	Point resultat;
		float xD1, yD1, xD2, yD2, xD3, yD3;
		float ua, ub, div;
 
		xD1 = dx2 - dx1;
		xD2 = sx2 - sx1;
		yD1 = dy2 - dy1;
		yD2 = sy2 - sy1;
		xD3 = dx1 - sx1;
		yD3 = dy1 - sy1;
  
		div = yD2*xD1 - xD2*yD1;
		ua = (xD2*yD3 - yD2*xD3) / div;
		ub = (xD1*yD3 - yD1*xD3) / div;
		resultat.X = dx1 + ua*xD1;
		resultat.Y = dy1 + ua*yD1;

		if (resultat.X >= sx1 && resultat.X <= sx2 || resultat.X <= sx1 && resultat.X >= sx2)
		{
			if (resultat.Y >= sy1 && resultat.Y <= sy2 || resultat.Y <= sy1 && resultat.Y >= sy2)
			{
				return true;
			}
		}

	return false;
}

Point NavMesh::RecupPointIntersectionSegmentDroite(float dx1, float dy1, float dx2, float dy2, float sx1, float sy1, float sx2, float sy2){
	Point resultat;
	float xD1, yD1, xD2, yD2, xD3, yD3;
	float ua, ub, div;

	xD1 = dx2 - dx1;
	xD2 = sx2 - sx1;
	yD1 = dy2 - dy1;
	yD2 = sy2 - sy1;
	xD3 = dx1 - sx1;
	yD3 = dy1 - sy1;
 
	div = yD2*xD1 - xD2*yD1;
	ua = (xD2*yD3 - yD2*xD3) / div;
	ub = (xD1*yD3 - yD1*xD3) / div;
	resultat.X = dx1 + ua*xD1;
	resultat.Y = dy1 + ua*yD1;

	return resultat;
}

bool NavMesh::EstEntreSegment(Point A, Point B, Point C){
	// le point C est entre A et B pour que ce soit vraie
	float S1X = B.X - A.X;
	float S1Y = B.Y - A.Y;
	float S2X = C.X - A.X;
	float S2Y = C.Y - A.Y;
	float produitVectoriel = S1X*S2Y - S2X*S1Y;
	if (produitVectoriel==0)
	{
		return true;
	}
	else
	{
		return false;
	}

}

std::vector < Polygone> NavMesh::CreerDeuxTriangles(int positionPolynome, Point A, std::vector<Polygone> listePolygone, Segment segment){
	/* A appartient � segment */
	std::vector < Polygone> lstPolygones(2);
	Point B;
	for (int l = 0; l < listePolygone[positionPolynome].Vertices.size(); l++)
	{
		if (!PointEstEgal(segment.A, listePolygone[positionPolynome].Vertices[l]))
		{
			if (!PointEstEgal(segment.B, listePolygone[positionPolynome].Vertices[l]))
			{
				B = listePolygone[positionPolynome].Vertices[l];
			}
		}
	}
			
	lstPolygones[0].Vertices.push_back(segment.A);
	lstPolygones[0].Vertices.push_back(A);
	lstPolygones[0].Vertices.push_back(B);
	lstPolygones[1].Vertices.push_back(A);
	lstPolygones[1].Vertices.push_back(segment.B);
	lstPolygones[1].Vertices.push_back(B);


	for (int w = 0; w < lstPolygones.size(); w++)
	{
		if (w == 0)
		{
			listePolygone[positionPolynome] = lstPolygones[w];
			std::cout << "OK1" << std::endl;
		}
		else
		{
			listePolygone.push_back(lstPolygones[w]);
			std::cout << "OK2" << std::endl;
		}
	}
	return listePolygone;
}

bool NavMesh::PointEstEgal(Point A, Point B){
	if (A.X==B.X)
	{
		if (A.Y==B.Y)
		{
			return true;
		}
	}
	return false;
}


/* pour calculer angle*/



/*

// calculates intersection and checks for parallel lines.
// also checks that the intersection point is actually on
// the line segment p1-p2
float xD1, yD1, xD2, yD2, xD3, yD3;
float dot, deg, len1, len2;
float segmentLen1, segmentLen2;
float ua, ub, div;

// calculate differences
xD1 = dx2 - dx1;
xD2 = sx2 - sx1;
yD1 = dy2 - dy1;
yD2 = sy2 - sy1;
xD3 = dx1 - sx1;
yD3 = dy1 - sy1;


// calculate angle between the two lines.
dot = (xD1*xD2 + yD1*yD2); // dot product
deg = dot / (len1*len2);



*/