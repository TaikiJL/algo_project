/*
Author: Taiki Hagiwara
*/

#ifndef Renderer_h__
#define Renderer_h__

#ifdef WIN32
#include <GL/glew.h>
#endif
#include <iostream>
#include <glm/mat4x4.hpp>
//#include <vector>
//#include <algorithm>
//#include <memory>

#include "Application.h"
#include "../assets/Mesh.h"
#include "../assets/Shader.h"

struct Geometry
{
	Geometry(const Mesh* mesh, const glm::mat4* matrix, const glm::vec4* color) : mesh(mesh), modelMatrix(matrix), color(color) {}
	const Mesh* mesh;
	const glm::mat4* modelMatrix;
	const glm::vec4* color;
};

struct Camera
{
	glm::mat4 viewMatrix;
	glm::mat4 projectionMatrix;
	glm::vec4 backgroundColor;
	std::vector<Geometry> geometriesList;
};

// Singleton class managing rendering.
class Renderer
{
public:

	Renderer() : _renderMode(GL_TRIANGLES) { if (!_singleton) _singleton = this; }
	~Renderer() {}

	static Renderer& Singleton() { return *_singleton; }

	int Initialize();
	int ShutDown();
	void Render();
	void RegisterCamera(const Camera& camera) { _camera = &camera; }
	void SetRenderMode(GLenum mode) { _renderMode = mode; }
	const Camera& GetCamera() const { return *_camera; }

private:

	static Renderer* _singleton;

	Shader _shader;
	const Camera* _camera;
	GLenum _renderMode;

};

#endif // Renderer_h__
