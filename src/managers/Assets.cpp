/*
Author: Taiki Hagiwara
*/

#include "Assets.h"

Assets* Assets::_singleton = nullptr;

void Assets::LoadOBJ(const char* path, float scale)
{
	FILE* file;
	fopen_s(&file, path, "r");
	if (file == NULL)
	{
		std::cerr << "Error : \"" << path << "\" cannot be opened.\n";
	}
	assert(file != NULL);

	std::vector<glm::vec3> vertices;
	std::vector<std::vector<int>> indices;
	std::vector<std::string> names;

	while (true)
	{
		char lineHead[256];

		// read the first word of the line
		int res = fscanf_s(file, "%s", lineHead, sizeof(lineHead));
		if (res == EOF)
			break;

		if (strcmp(lineHead, "v") == 0)
		{
			glm::vec3 vertex;
			fscanf_s(file, "%f %f %f\n", &vertex.x, &vertex.y, &vertex.z);
			vertices.push_back(vertex * scale);
		}
		else if (strcmp(lineHead, "o") == 0 || strcmp(lineHead, "g") == 0)
		{
			indices.push_back(std::vector<int>());
			char meshName[512];
			fscanf_s(file, "%s\n", meshName, sizeof(meshName));
			names.push_back(std::string(meshName));
		}
		else if (strcmp(lineHead, "f") == 0)
		{
			int faceIndices[3];
			int matches = fscanf_s(file, "%d %d %d\n",
				&faceIndices[0], &faceIndices[1], &faceIndices[2]);
			if (matches != 3)
			{
				std::cerr << "File can't be read by our simple parser : ( Try exporting with other options" << std::endl;
			}
			assert(matches == 3);

			// Indexes for .obj faces start at 1. Our array's start at 0.
			for (auto index : faceIndices)
				indices.back().push_back(index - 1);
		}
	}

	std::vector<int> vertexPositions(vertices.size(), -1);
	for (int i = 0; i < names.size(); ++i)
	{
		std::vector<glm::vec3> meshVertices;
		std::vector<unsigned int> meshIndices;
		meshVertices.reserve(vertices.size());
		meshIndices.reserve(indices[i].size());
		for (auto index : indices[i])
		{
			int& vertexPosition = vertexPositions[index];
			if (vertexPosition != -1)
			{
				meshIndices.push_back(vertexPosition);
			}
			else
			{
				meshVertices.push_back(vertices[index]);
				unsigned int newPosition = meshVertices.size() - 1;
				meshIndices.push_back(newPosition);
				vertexPosition = newPosition;
			}
		}
		std::string meshName = std::string(path) + "/" + names[i];
		meshVertices.shrink_to_fit();
		Mesh mesh;
		mesh.LoadVertices(std::move(meshVertices), std::move(meshIndices));
		_meshes.insert(std::make_pair(std::move(meshName), std::move(mesh)));

		std::fill(vertexPositions.begin(), vertexPositions.end(), -1);
	}

	fclose(file);
}
