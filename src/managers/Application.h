/*
Author: Taiki Hagiwara
*/

#ifndef Application_h__
#define Application_h__

#include <iostream>
#include <string>
#include <vector>
#include <functional>

// Singleton managing the application.
class Application
{
public:

	Application() { if (!_singleton) _singleton = this; }
	~Application() {}

	static Application& Singleton() { return *_singleton; }

	void Initialize();
	void ShutDown() {}

	bool IsRunning() const { return _isRunning; }
	void IsRunning(bool val) { _isRunning = val; }
	int RenderWidth() const { return _renderWidth; }
	int RenderHeight() const { return _renderHeight; }
	void SetResolution(int width, int  height);
	void RegisterResolutionChangeCallback(std::function<void(int, int)> callBack) { _onResolutionChanged.push_back(callBack); }

private:

	static Application* _singleton;

	bool _isRunning;
	int _renderWidth;
	int _renderHeight;
	std::vector<std::function<void(int, int)>> _onResolutionChanged;
	
};

#endif // Application_h__
