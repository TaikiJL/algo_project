/*
Author: Taiki Hagiwara
*/

#pragma once

#include <unordered_map>
#include <string>
#include <iostream>
#include <cstring>
#include <cassert>
#include <vector>
#include <algorithm>

#include "../assets/Mesh.h"

class Assets
{

public:

	Assets() { if (!_singleton) _singleton = this; }
	~Assets() {}

	static Assets& Singleton() { return *_singleton; }

	void AddMesh(std::string& name, Mesh& mesh) { _meshes.insert(std::make_pair(std::move(name), std::move(mesh))); }
	Mesh& RetrieveMesh(const std::string& name) { return _meshes.at(name); }
	void LoadOBJ(const char* path, float scale = 0.01f);

private:

	static Assets* _singleton;

	std::unordered_map<std::string, Mesh> _meshes;

};
