/*
Author: Taiki Hagiwara
*/

#include "Renderer.h"

Renderer* Renderer::_singleton = nullptr;

int Renderer::Initialize()
{
#ifdef WIN32
	// GLEW initialization
	GLenum glewInit(glewInit());

	if (glewInit != GLEW_OK)
	{
		std::cout << "GLEW initialization error: " << glewGetErrorString(glewInit) << std::endl;
		return -1;
	}
#endif

	glEnable(GL_DEPTH_TEST);

	_shader.Load("data/shaders/couleur3D.vert", "data/shaders/couleur3D.frag");

	Application::Singleton().RegisterResolutionChangeCallback([](int w, int h){ glViewport(0, 0, w, h); });

	return 0;
}

int Renderer::ShutDown()
{


	return 0;
}

void Renderer::Render()
{
	glUseProgram(_shader.ProgramID());
	
	const glm::vec4& bgColor = _camera->backgroundColor;
	glClearColor(bgColor.r, bgColor.g, bgColor.b, bgColor.a);
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	glUniformMatrix4fv(glGetUniformLocation(_shader.ProgramID(), "projection"),
		1,
		GL_TRUE,
		(GLfloat*)&(_camera->projectionMatrix));

	const glm::mat4& viewMatrix = _camera->viewMatrix;

	for (const auto& geometry : _camera->geometriesList)
	{
		glUniform4fv(glGetUniformLocation(_shader.ProgramID(), "color"),
			1,
			(GLfloat*)geometry.color);

		const glm::mat4& modelMatrix = *(geometry.modelMatrix);
		glm::mat4 modelView = viewMatrix * modelMatrix;

		glUniformMatrix4fv(glGetUniformLocation(_shader.ProgramID(), "modelview"),
			1,
			GL_TRUE,
			(GLfloat*)&modelView);

		glBindVertexArray(geometry.mesh->VaoID());

		glDrawElements(_renderMode, geometry.mesh->/*WireframeIndexCount()*/WireframeIndexCount(), GL_UNSIGNED_INT, 0);

		glBindVertexArray(0);
	}

	glUseProgram(0);
	
}
