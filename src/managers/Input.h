/*
Author: Taiki Hagiwara
*/

#ifndef Input_h__
#define Input_h__

#include <vector>
#include <functional>

#include "Application.h"
#include "Window.h"

class Input
{
public:

	Input() { if (!_singleton) _singleton = this; }
	~Input() {}

	static Input& Singleton() { return *_singleton; }

	int Initialize() { return 0; }
	int ShutDown() { return 0; }
	void Update();
	void RegisterKeyCommand(SDL_Keycode key, SDL_EventType eventType, std::function<void()> func) { _keyCommands.push_back(KeyCommand(key, eventType, func)); }
	void RegisterMouseCommand(Uint8 button, SDL_EventType eventType, std::function<void()> func) { _mouseCommands.push_back(MouseCommand(button, eventType, func)); }

private:

	static Input* _singleton;

	struct KeyCommand
	{
		KeyCommand(SDL_Keycode key, SDL_EventType eventType, std::function<void()> func) : key(key), eventType(eventType), func(func) {}
		SDL_Keycode key;
		SDL_EventType eventType;
		std::function<void()> func;
	};
	struct MouseCommand
	{
		MouseCommand(Uint8 button, SDL_EventType eventType, std::function<void()> func) : button(button), eventType(eventType), func(func) {}
		Uint8 button;
		SDL_EventType eventType;
		std::function<void()> func;
	};

	std::vector<KeyCommand> _keyCommands;
	std::vector<MouseCommand> _mouseCommands;

};

#endif // Input_h__
