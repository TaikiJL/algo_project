/*
Author: Taiki Hagiwara
*/

#include "Window.h"

Window* Window::_singleton = nullptr;

int Window::Initialize()
{
	// Initialization of SDL
	if (SDL_Init(SDL_INIT_VIDEO) < 0)
	{
		std::cout << "Error during SDL initialization: " << SDL_GetError() << std::endl;
		SDL_Quit();
		return -1;
	}

	// OpenGL version
	SDL_GL_SetAttribute(SDL_GL_CONTEXT_MAJOR_VERSION, 3);
	SDL_GL_SetAttribute(SDL_GL_CONTEXT_MINOR_VERSION, 1);

	// Double Buffer
	SDL_GL_SetAttribute(SDL_GL_DOUBLEBUFFER, 1);
	SDL_GL_SetAttribute(SDL_GL_DEPTH_SIZE, 24);
	
	// Window creation
	_window = SDL_CreateWindow("Projet d'algorithmique : Mesh de navigation",
		SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED,
		Application::Singleton().RenderWidth(), Application::Singleton().RenderHeight(),
		SDL_WINDOW_SHOWN | SDL_WINDOW_OPENGL);

	Application::Singleton().RegisterResolutionChangeCallback([&](int w, int h){
		SDL_SetWindowSize(this->_window, w, h);
		SDL_SetWindowPosition(this->_window, SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED);
	});

	if (_window == 0)
	{
		std::cout << "Error during window creation: " << SDL_GetError() << std::endl;
		SDL_Quit();
		return -1;
	}

	// OpenGL context creation
	_openGLContext = SDL_GL_CreateContext(_window);

	if (_openGLContext == 0)
	{
		std::cout << SDL_GetError() << std::endl;
		SDL_DestroyWindow(_window);
		SDL_Quit();
		return -1;
	}

	return 0;
}

int Window::ShutDown()
{
	SDL_GL_DeleteContext(_openGLContext);
	SDL_DestroyWindow(_window);
	SDL_Quit();

	return 0;
}

int Window::PollEvent()
{
	int pending = SDL_PollEvent(&_event);

	if (_event.window.event == SDL_WINDOWEVENT_CLOSE)
		Application::Singleton().IsRunning(false);

	return pending;
}

void Window::Swapbuffers()
{
	SDL_GL_SwapWindow(_window);
	//SDL_GL_SetSwapInterval(0);
}

void Window::Screen2Viewport(int x, int y, float* viewportX, float *viewportY)
{
	*viewportX = ((2.f * (float)x) / Application::Singleton().RenderWidth()) - 1.f;
	*viewportY = 1.f - ((2.f * (float)y) / Application::Singleton().RenderHeight());
}
