/*
Author: Taiki Hagiwara
*/

#ifndef Time_h__
#define Time_h__

#include <chrono>

typedef std::chrono::high_resolution_clock HRClock;
typedef std::chrono::duration<float, std::milli> Duration;

class Time
{
public:

	Time() : _deltaTime(0.f) { if (!_singleton) _singleton = this; }
	~Time() {}

	static Time& Singleton() { return *_singleton; }
	static float DeltaTime() { return _singleton->_deltaTime; }
	static float ElapsedTime() { return _singleton->_elapsedTime; }

	void Initialize();
	void UpdateDeltaTime();
	void ResetTimer();

private:

	static Time* _singleton;

	float _deltaTime;
	float _elapsedTime;
	std::chrono::time_point<std::chrono::system_clock, std::chrono::system_clock::duration> _previousTime;

};

#endif // Time_h__
