/*
Author: Taiki Hagiwara
*/

#include "Application.h"

Application* Application::_singleton = nullptr;

void Application::SetResolution(int width, int height)
{
	_renderWidth = width;
	_renderHeight = height;
	for (auto& callBack : _onResolutionChanged)
		callBack(width, height);
}

void Application::Initialize()
{
	_isRunning = true;
}
