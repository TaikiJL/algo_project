/*
Author: Taiki Hagiwara
*/

#ifndef Window_h__
#define Window_h__

#include <SDL/SDL.h>
#include <functional>

#include "Application.h"

class Window
{
public:

	Window() { if (!_singleton) _singleton = this; }
	~Window() {}

	static Window& Singleton() { return *_singleton; }

	int Initialize();
	int ShutDown();
	int PollEvent();
	void Swapbuffers();
	const SDL_Event& Event() const { return _event; }
	void Screen2Viewport(int x, int y, float* viewportX, float* viewportY);

private:

	static Window* _singleton;

	SDL_Window* _window;
	SDL_GLContext _openGLContext;
	SDL_Event _event;

};

#endif // Window_h__
