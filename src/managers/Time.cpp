/*
Author: Taiki Hagiwara
*/

#include "Time.h"

Time* Time::_singleton = nullptr;

void Time::Initialize()
{
	_previousTime = HRClock::now();
}

void Time::UpdateDeltaTime()
{
	Duration deltaTime = HRClock::now() - _previousTime;
	_deltaTime = 0.001f * deltaTime.count();
	_previousTime = HRClock::now();
	_elapsedTime += _deltaTime;
}

void Time::ResetTimer()
{
	_elapsedTime = 0.f;
}
