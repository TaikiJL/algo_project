/*
Author: Taiki Hagiwara
*/

#include "Input.h"

Input* Input::_singleton = nullptr;

void Input::Update()
{
	const SDL_Event& event = Window::Singleton().Event();
	
	while (Window::Singleton().PollEvent())
	{
		if (event.window.event == SDL_WINDOWEVENT_CLOSE)
			Application::Singleton().IsRunning(false);

		for (const auto& command : _mouseCommands)
		{
			if (event.type == command.eventType &&
				event.button.button == command.button)
				command.func();
		}

		for (const auto& command : _keyCommands)
		{
			if (event.type == command.eventType &&
				event.key.keysym.sym == command.key &&
				event.key.repeat == 0)
				command.func();
		}
	}
}
