/*

Adrien Deban
Nicolas Bordas
Taiki Hagiwara
Jeremy Wurtzel

Projet final
8INF870 - Algorithmique

Mesh de navigation

*/

#pragma once

#include "objects/Object.h"
#include "objects/ObjectData.h"

class Terrain : public Object
{

public:

	static std::shared_ptr<Object> CreationTerrain(const ObjectData& donnees);

	Terrain() {}
	~Terrain() {}

	void Initialize() override {}
	void Update() override {}

private:



};
