/*
Author: Taiki Hagiwara
*/

#ifndef Mesh_h__
#define Mesh_h__

#ifdef WIN32
#include <GL/glew.h>
#endif
#include <glm/vec3.hpp>
#include <vector>

struct Vertex
{

	Vertex() {}
	Vertex(const glm::vec3& position) : position(position) {}

	glm::vec3 position;

};

class Mesh
{

public:

	Mesh() : _vaoID(0), _vboID(0), _iboID(0), _refCount(0) {}
	Mesh(Mesh&& other);
	~Mesh() { DestroyBuffers(); }

	const std::vector<glm::vec3>& Vertices() const { return _vertices; }
	const std::vector<unsigned int>& Indices() const { return _indices; }
	void LoadVertices(const std::vector<glm::vec3>& vertices, const std::vector<unsigned int>& indices);
	void LoadVertices(std::vector<glm::vec3>&& vertices, std::vector<unsigned int>&& indices);
	void LoadLineVertices(std::vector<glm::vec3>&& vertices, std::vector<unsigned int>&& indices) { _vertices = vertices; _wireframeIndices = indices; DestroyBuffers(); _refCount = 0; }
	GLuint VaoID() const { return _vaoID; }
	int IndexCount() const { return _indices.size(); }
	int WireframeIndexCount() const { return _wireframeIndices.size(); }
	void IncrementRefCount();
	void DecrementRefCount();

private:

	std::vector<glm::vec3> _vertices;
	std::vector<unsigned int> _indices;
	std::vector<unsigned int> _wireframeIndices;
	GLuint _vaoID;
	GLuint _vboID;
	GLuint _iboID;
	int _refCount;

	void LoadToGPU();
	void DestroyBuffers();
	void GenerateWireframeIndices();
	
};

#endif // Mesh_h__
