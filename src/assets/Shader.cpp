/*
Author: Taiki Hagiwara
*/

#include "Shader.h"

Shader::~Shader()
{
	glDeleteShader(_vertexID);
	glDeleteShader(_fragmentID);
	glDeleteProgram(_programID);
}

bool Shader::Load(const char* vertexSource, const char* fragmentSource)
{
	// Deleting shaders if they already exists
	if (glIsShader(_vertexID) == GL_TRUE)
		glDeleteShader(_vertexID);

	if (glIsShader(_fragmentID) == GL_TRUE)
		glDeleteShader(_fragmentID);

	if (glIsProgram(_programID) == GL_TRUE)
		glDeleteProgram(_programID);

	// Compiling shaders
	if (!Compile(_vertexID, GL_VERTEX_SHADER, vertexSource))
		return false;

	if (!Compile(_fragmentID, GL_FRAGMENT_SHADER, fragmentSource))
		return false;

	// Creates program
	_programID = glCreateProgram();

	// Associates shaders
	glAttachShader(_programID, _vertexID);
	glAttachShader(_programID, _fragmentID);

	// Attrib locations boundings
	glBindAttribLocation(_programID, 0, "in_Vertex");

	// Program linkage
	glLinkProgram(_programID);

	GLint linkError(0);
	glGetProgramiv(_programID, GL_LINK_STATUS, &linkError);

	if (linkError != GL_TRUE)
	{
		GLint errorSize(0);
		glGetProgramiv(_programID, GL_INFO_LOG_LENGTH, &errorSize);

		char* error = new char[errorSize + 1];

		glGetShaderInfoLog(_programID, errorSize, &errorSize, error);
		error[errorSize] = '\0';

		std::cout << error << std::endl;

		delete[] error;
		glDeleteProgram(_programID);

		return false;
	}
	else
		return true;
}

bool Shader::Compile(GLuint& shader, GLenum type, const char* sourceFile)
{
	// Creates shader
	shader = glCreateShader(type);

	if (shader == 0)
	{
		std::cout << "Error: shader type (" << type << ") does not exist!" << std::endl;
		return false;
	}

	std::ifstream file(sourceFile);

	if (!file)
	{
		std::cout << "Error: " << sourceFile << " cannot be found!" << std::endl;
		glDeleteShader(shader);

		return false;
	}

	std::string line;
	std::string sourceCode;

	while (getline(file, line))
		sourceCode += line + '\n';

	file.close();

	const GLchar* sourceCodeString = sourceCode.c_str();

	glShaderSource(shader, 1, &sourceCodeString, 0);

	glCompileShader(shader);

	GLint compileError(0);
	glGetShaderiv(shader, GL_COMPILE_STATUS, &compileError);

	if (compileError != GL_TRUE)
	{
		GLint errorSize(0);
		glGetShaderiv(shader, GL_INFO_LOG_LENGTH, &errorSize);

		char* error = new char[errorSize + 1];

		glGetShaderInfoLog(shader, errorSize, &errorSize, error);
		error[errorSize] = '\0';

		std::cout << error << std::endl;

		delete[] error;
		glDeleteShader(shader);

		return false;
	}
	else
		return true;
}
