/*
Author: Taiki Hagiwara
*/

#pragma once

#ifdef WIN32
#include <GL/glew.h>
#endif
#include <iostream>
#include <string>
#include <fstream>

class Shader
{

public:

	Shader() :_programID(0), _vertexID(0), _fragmentID(0) {}
	~Shader();

	bool Load(const char* vertexSource, const char* fragmentSource);
	GLuint ProgramID() const { return _programID; }

private:

	GLuint _vertexID;
	GLuint _fragmentID;
	GLuint _programID;

	bool Compile(GLuint& shader, GLenum type, const char* sourceFile);

};
