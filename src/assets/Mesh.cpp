/*
	Author: Taiki Hagiwara
*/

#include "Mesh.h"

#ifndef BUFFER_OFFSET
#define BUFFER_OFFSET(offset) ((char*)NULL + (offset))
#endif

/*
Mesh::Mesh()
{
	_vertices = {
		{ -0.5f, -0.5f, 0.f },
		{ 0.5f, -0.5f, 0.f },
		{ -0.5f, 0.5f, 0.f },
		{ 0.5f, 0.5f, 0.f }
	};
	
	_indices = { 2, 0, 1, 3, 2, 1 };
	
	LoadToGPU();
}
*/

Mesh::Mesh(Mesh&& other) : _vertices(std::move(other._vertices)), _indices(std::move(other._indices)), _wireframeIndices(std::move(other._wireframeIndices)),
_vaoID(other._vaoID), _vboID(other._vaoID), _iboID(other._iboID), _refCount(other._refCount)
{
	other._vaoID = 0;
	other._vboID = 0;
	other._iboID = 0;
}

void Mesh::LoadVertices(const std::vector<glm::vec3>& vertices, const std::vector<unsigned int>& indices)
{
	_vertices = vertices;
	_indices = indices;
	GenerateWireframeIndices();
	DestroyBuffers();
	_refCount = 0;
}

void Mesh::LoadVertices(std::vector<glm::vec3>&& vertices, std::vector<unsigned int>&& indices)
{
	_vertices = vertices;
	_indices = indices;
	GenerateWireframeIndices();
	DestroyBuffers();
	_refCount = 0;
}

void Mesh::LoadToGPU()
{
	// Vertex array
	if (glIsVertexArray(_vaoID) == GL_TRUE)
		glDeleteVertexArrays(1, &_vaoID);

	glGenVertexArrays(1, &_vaoID);

	glBindVertexArray(_vaoID);

	// Vertex buffer
	if (glIsBuffer(_vboID) == GL_TRUE)
		glDeleteBuffers(1, &_vboID);

	glGenBuffers(1, &_vboID);

	glBindBuffer(GL_ARRAY_BUFFER, _vboID);

	glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, sizeof(Vertex), BUFFER_OFFSET(0));
	glEnableVertexAttribArray(0);

	glBufferData(GL_ARRAY_BUFFER, sizeof(Vertex) * _vertices.size(), &_vertices[0], GL_STATIC_DRAW);

	// Index buffer
	if (glIsBuffer(_iboID) == GL_TRUE)
		glDeleteBuffers(1, &_iboID);

	glGenBuffers(1, &_iboID);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, _iboID);
	glBufferData(GL_ELEMENT_ARRAY_BUFFER, /*_indices.size()*/_wireframeIndices.size() * sizeof(unsigned int), &_wireframeIndices[0], GL_STATIC_DRAW);

	glBindVertexArray(0);

	glBindBuffer(GL_ARRAY_BUFFER, 0);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);
}

void Mesh::DestroyBuffers()
{
	if (glIsBuffer(_vboID))
		glDeleteBuffers(1, &_vboID);
	if (glIsBuffer(_iboID))
		glDeleteBuffers(1, &_iboID);

	if (glIsVertexArray(_vaoID))
		glDeleteVertexArrays(1, &_vaoID);
}

void Mesh::GenerateWireframeIndices()
{
	_wireframeIndices.clear();
	_wireframeIndices.reserve(_indices.size() * 2);

	for (int i = 0; i < _indices.size(); ++i)
	{
		if ((i + 1) % 3 == 0)
		{
			_wireframeIndices.push_back(_indices[i]);
			_wireframeIndices.push_back(_indices[i - 2]);
		}
		else
		{
			_wireframeIndices.push_back(_indices[i]);
			_wireframeIndices.push_back(_indices[i + 1]);
		}
	}
}

void Mesh::IncrementRefCount()
{
	if (++_refCount > 1)
		return;

	// If the mesh wasn't sent to the GPU, do it
	LoadToGPU();
}

void Mesh::DecrementRefCount()
{
	if (--_refCount > 0)
		return;
	else
		_refCount = 0;

	// If the mesh isn't referenced by the GPU anymore, delete from it
	DestroyBuffers();
}
