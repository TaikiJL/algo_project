/*

Adrien Deban
Nicolas Bordas
Taiki Hagiwara
Jeremy Wurtzel

Projet final
8INF870 - Algorithmique

Mesh de navigation

*/

#include "Scene.h"

Scene* Scene::_current = nullptr;

const ObjectCreationMap* Scene::_objectCreators = nullptr;

const std::unordered_map<std::string, Scene::ComponentType> Scene::COMPONENT_TYPES = {
	{ "transform", ComponentType::TRANSFORM },
	{ "graphics", ComponentType::GRAPHICS },
	{ "camera", ComponentType::CAMERA },
};

Scene::Scene(const char* filePath)
{
	if (!_current) _current = this;

	tinyxml2::XMLDocument sceneFile;
	sceneFile.LoadFile(filePath);

	const tinyxml2::XMLElement* sceneRoot = sceneFile.FirstChildElement("scene");

	LoadSettings(sceneRoot);
	LoadAssets(sceneRoot);
	LoadObjects(sceneRoot);
}

void Scene::Initialize()
{
	for (auto object : _objects) object.second->Initialize();

	for (auto& ptr : _cameras)
	{
		std::shared_ptr<CameraComponent> camera(ptr);
		camera->UpdateRenderList(_graphics);
	}
}

void Scene::Update()
{
	for (auto object : _objects) object.second->Update();

	for (auto& ptr : _transforms)
	{
		std::shared_ptr<TransformComponent> transform(ptr);
		transform->Update();
	}
	for (auto& ptr : _cameras)
	{
		std::shared_ptr<CameraComponent> camera(ptr);
		camera->Update();
		camera->UpdateRenderList(_graphics);
	}
}

void Scene::AddObject(const std::string& name, std::shared_ptr<Object> object)
{
	auto nameQueryResult = _objects.find(name);
	if (nameQueryResult != _objects.end())
	{
		std::cerr << "Error: duplicate object name for \"" << name << "\"" << std::endl;
	}
	assert(nameQueryResult == _objects.end());

	_objects.insert(std::make_pair(name, object));
}

void Scene::LoadSettings(const tinyxml2::XMLElement* const sceneRootNode)
{
	const tinyxml2::XMLElement* const settingsRoot = sceneRootNode->FirstChildElement("settings");

	if (!settingsRoot) return;

	const tinyxml2::XMLElement* resolution = settingsRoot->FirstChildElement("resolution");
	if (resolution)
		Application::Singleton().SetResolution(resolution->IntAttribute("width"), resolution->IntAttribute("height"));

	const tinyxml2::XMLElement* renderMode = settingsRoot->FirstChildElement("renderMode");
	if (renderMode)
	{
		std::string renderModeName(renderMode->GetText());
		if (renderModeName.compare("shaded") == 0)
			Renderer::Singleton().SetRenderMode(/*GL_TRIANGLES*/GL_LINES);
		else if (renderModeName.compare("wireframe") == 0)
			Renderer::Singleton().SetRenderMode(GL_LINES);
	}
}

void Scene::LoadAssets(const tinyxml2::XMLElement* const sceneRootNode)
{
	const tinyxml2::XMLElement* const assetsRoot = sceneRootNode->FirstChildElement("assets");

	if (!assetsRoot) return;

	Assets& assetManager = Assets::Singleton();

	const tinyxml2::XMLElement* currentMesh = assetsRoot->FirstChildElement("mesh");

	if (!currentMesh) return;

	do
	{
		float scale = 0.01f;
		currentMesh->QueryFloatAttribute("scale", &scale);

		assetManager.LoadOBJ(currentMesh->GetText(), scale);
	}
	while (currentMesh = currentMesh->NextSiblingElement("mesh"));
}

void Scene::LoadObjects(const tinyxml2::XMLElement* const sceneRootNode)
{
	const tinyxml2::XMLElement* currentObject = sceneRootNode->FirstChildElement("object");

	if (!currentObject) return;

	unsigned int unnamedObjectsIndex = 0;

	do
	{
		const tinyxml2::XMLElement* objectName = currentObject->FirstChildElement("name");
		std::string name;
		if (!objectName) name = "unnamedObject" + std::to_string(unnamedObjectsIndex++);
		else name = objectName->GetText();

		auto nameQueryResult = _objects.find(name);
		if (nameQueryResult != _objects.end())
		{
			std::cerr << "Error: duplicate object name for \"" << name << "\"" << std::endl;
		}
		assert(nameQueryResult == _objects.end());

		std::shared_ptr<Object> object;

		const char* objectType = currentObject->Attribute("type");
		if (objectType)
		{
			auto objectCreator = _objectCreators->find(objectType);
			if (objectCreator == _objectCreators->end())
			{
				std::cout << "The object type  \"" << objectType << "\" is not known. This object is skipped." << std::endl;
				continue;
			}

			ObjectData objectData;
			objectData.FillData(currentObject);
			object = objectCreator->second(objectData);
		}
		else
		{
			object = std::make_shared<Object>();
		}

		_objects.insert(std::make_pair(name, object));

		const tinyxml2::XMLElement* currentComponent = currentObject->FirstChildElement("component");
		if (!currentComponent) continue;

		do
		{
			const char* componentTypeName = currentComponent->Attribute("type");
			auto componentType = COMPONENT_TYPES.find(componentTypeName);
			if (componentType == COMPONENT_TYPES.end())
			{
				std::cout << "The component type \"" << componentTypeName << "\" is not known. This component is skipped." << std::endl;
				continue;
			}
			
			std::shared_ptr<Component> component;
			switch (componentType->second)
			{
			case Scene::ComponentType::TRANSFORM:
			{
				component = object->AddComponent<TransformComponent>();
				_transforms.push_back(std::weak_ptr<TransformComponent>(std::dynamic_pointer_cast<TransformComponent, Component>(component)));
			}
				break;
			case Scene::ComponentType::GRAPHICS:
			{
				component = object->AddComponent<GraphicsComponent>();
				_graphics.push_back(std::weak_ptr<GraphicsComponent>(std::dynamic_pointer_cast<GraphicsComponent, Component>(component)));
			}
				break;
			case Scene::ComponentType::CAMERA:
			{
				component = object->AddComponent<CameraComponent>();
				_cameras.push_back(std::weak_ptr<CameraComponent>(std::dynamic_pointer_cast<CameraComponent, Component>(component)));
			}
				break;
			default:
				break;
			}

			ObjectData componentData;
			componentData.FillData(currentComponent);
			component->LoadData(componentData);
		}
		while (currentComponent = currentComponent->NextSiblingElement("component"));
	}
	while (currentObject = currentObject->NextSiblingElement("object"));
}
