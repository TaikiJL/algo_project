/*

Adrien Deban
Nicolas Bordas
Taiki Hagiwara
Jeremy Wurtzel

Projet final
8INF870 - Algorithmique

Mesh de navigation

*/

#include "Obstacle.h"

std::shared_ptr<Object> Obstacle::CreationObstacle(const ObjectData& donnees)
{
	auto obstacle = std::make_shared<Obstacle>();
	auto ptr = std::dynamic_pointer_cast<Object, Obstacle>(obstacle);
	return ptr;
}
