/*

Adrien Deban
Nicolas Bordas
Taiki Hagiwara
Jeremy Wurtzel

Projet final
8INF870 - Algorithmique

Mesh de navigation

*/

#include "Terrain.h"

std::shared_ptr<Object> Terrain::CreationTerrain(const ObjectData& donnees)
{
	auto terrain = std::make_shared<Terrain>();
	auto ptr = std::dynamic_pointer_cast<Object, Terrain>(terrain);
	return ptr;
}
