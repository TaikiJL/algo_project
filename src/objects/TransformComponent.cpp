/*
Author: Taiki Hagiwara
*/

#include "TransformComponent.h"

void TransformComponent::LoadData(const ObjectData& data)
{
	const auto& positionData = data.GetStruct("position");
	_position = glm::vec3(positionData.GetFloatVar("x"), positionData.GetFloatVar("y"), positionData.GetFloatVar("z"));

	const auto& scaleData = data.GetStruct("scale");
	_scale = glm::vec3(scaleData.GetFloatVar("x"), scaleData.GetFloatVar("y"), scaleData.GetFloatVar("z"));

	_isDirty = true;
	Update();
}

void TransformComponent::Update()
{
	if (_isDirty)
	{
		//glm::mat4 scale = glm::mat4(_scale.x, 0.f, 0.f, 0.f,
		//	0.f, _scale.y, 0.f, 0.f,
		//	0.f, 0.f, _scale.z, 0.f,
		//	0.f, 0.f, 0.f, 1.f);

		glm::mat4 translate = glm::mat4(1.f, 0.f, 0.f, _position.x,
			0.f, 1.f, 0.f, _position.y,
			0.f, 0.f, 1.f, _position.z,
			0.f, 0.f, 0.f, 1.f);

		_world = translate/* * scale*/;

		for (auto callback : _onCleansed) callback();

		_isDirty = false;
	}
}
