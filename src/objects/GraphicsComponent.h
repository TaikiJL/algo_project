/*
Author: Taiki Hagiwara
*/

#pragma once

#include <glm/vec4.hpp>
#include <glm/mat4x4.hpp>

#include "Component.h"
#include "Object.h"
#include "../assets/Mesh.h"
#include "../managers/Assets.h"

class GraphicsComponent : public Component
{

public:

	GraphicsComponent(Object& owner) : Component(owner) { _modelMatrix = &(owner.Transform()->World()); }
	~GraphicsComponent() {}

	void LoadData(const ObjectData& data) override;
	void Update() override {}

	const Mesh* GetMesh() const { return _mesh; }
	void SetMesh(Mesh* mesh) { _mesh = mesh; }
	void ReferenceMesh() { _mesh->IncrementRefCount(); }
	void UnreferenceMesh() { _mesh->DecrementRefCount(); }
	const glm::mat4* ModelMatrix() const { return _modelMatrix; }
	const glm::vec4& Color() const { return _color; }
	void Color(const glm::vec4& color) { _color = color; }

private:

	glm::vec4 _color;
	const glm::mat4* _modelMatrix;
	Mesh* _mesh;

};
