/*
Author: Taiki Hagiwara
*/

#ifndef TransformComponent_h__
#define TransformComponent_h__

#include <glm/vec3.hpp>
#include <glm/mat4x4.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <functional>
#include <vector>

#include "Component.h"

class TransformComponent : public Component
{

public:

	TransformComponent(Object& owner) : Component(owner), _isDirty(true), _scale(glm::vec3(1.f)) {}
	~TransformComponent() {}

	void LoadData(const ObjectData& data) override;
	void Update() override;

	const glm::mat4& World() const { return _world; }
	const glm::vec3& Position() const { return _position; }
	void Position(const glm::vec3& position) { _position = position; _isDirty = true; }
	const glm::vec3& Scale() const { return _scale; }
	void Scale(const glm::vec3& scale) { _scale = scale; _isDirty = true; }
	void RegisterOnCleansedCallback(std::function<void()> callback) { _onCleansed.push_back(callback); }
	void Translate(const glm::vec3& translationVector) { _position += translationVector; _isDirty = true; }

private:

	glm::mat4 _world;
	glm::vec3 _position;
	glm::vec3 _scale;
	
	bool _isDirty;
	std::vector<std::function<void()>> _onCleansed;

};

#endif // TransformComponent_h__
