/*
Author: Taiki Hagiwara
*/

#pragma once

#include <memory>
#include <glm/vec4.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/matrix_inverse.hpp>
#include <vector>

#include "Component.h"
#include "Object.h"
#include "GraphicsComponent.h"
#include "../managers/Renderer.h"
#include "../managers/Application.h"
#include "../managers/Window.h"

class CameraComponent : public Component
{

public:

	CameraComponent(Object& owner);
	~CameraComponent() {}

	static CameraComponent& Main() { return *_mainCamera; }

	void LoadData(const ObjectData& data) override;
	void Update() override;
	void UpdateRenderList(const std::vector<std::weak_ptr<GraphicsComponent>>& graphics);

	bool IsOrthographic() const { return _isOrthographic; }
	void SetToOrthographic() { _isOrthographic = true; _isDirty = true; }
	void SetToPerspective() { _isOrthographic = false; _isDirty = true; }
	float Size() const { return _size; }
	void Size(float size) { _size = size; _isDirty = true; }
	const glm::vec4& BackgroundColor() const { return _camera.backgroundColor; }
	void BackgroundColor(const glm::vec4& color) { _camera.backgroundColor = color; }
	glm::vec3 Screen2World(int x, int y, float desiredZ) const;

private:

	static CameraComponent* _mainCamera;

	std::vector<std::weak_ptr<GraphicsComponent>> _previousRenderList;
	Camera _camera;
	float _nearClip;
	float _farClip;
	float _fov;
	float _size;
	bool _isOrthographic;
	bool _isDirty;

};
