/*
Author: Taiki Hagiwara
*/

#include "ObjectData.h"

ObjectData& ObjectData::operator=(ObjectData&& other)
{
	_strings = std::move(other._strings);
	_objects = std::move(other._objects);
	_floats = std::move(other._floats);
	_ints = std::move(other._ints);
	_bools = std::move(other._bools);
	return *this;
}

void ObjectData::FillData(const tinyxml2::XMLElement* node)
{
	const tinyxml2::XMLElement* stringNode = node->FirstChildElement("string");
	if (stringNode)
	{
		do
		{
			_strings[stringNode->Attribute("name")] = stringNode->GetText();
		} while (stringNode = stringNode->NextSiblingElement("string"));
	}

	const tinyxml2::XMLElement* floatNode = node->FirstChildElement("float");
	if (floatNode)
	{
		do
		{
			_floats[floatNode->Attribute("name")] = (float)std::atof(floatNode->GetText());
		} while (floatNode = floatNode->NextSiblingElement("float"));
	}

	const tinyxml2::XMLElement* intNode = node->FirstChildElement("int");
	if (intNode)
	{
		do
		{
			_ints[intNode->Attribute("name")] = std::atoi(intNode->GetText());
		} while (intNode = intNode->NextSiblingElement("int"));
	}

	const tinyxml2::XMLElement* boolNode = node->FirstChildElement("bool");
	if (boolNode)
	{
		do
		{
			_bools[boolNode->Attribute("name")] = std::strcmp(boolNode->GetText(), "true") == 0 ? true : false;
		} while (boolNode = boolNode->NextSiblingElement("bool"));
	}

	const tinyxml2::XMLElement* objectNode;
	if (objectNode = node->FirstChildElement("struct"))
	{
		do
		{
			std::string objName = objectNode->Attribute("name");
			ObjectData object;
			object.FillData(objectNode);
			_objects[objName] = std::move(object);
		} while (objectNode = objectNode->NextSiblingElement("struct"));
	}
}
