/*
Author: Taiki Hagiwara
*/

#ifndef Object_h__
#define Object_h__

#include <string>
#include <vector>
#include <memory>

#include "../third_party/tinyxml2/tinyxml2.h"
#include "Component.h"
#include "TransformComponent.h"

class Object
{

public:

	Object() : _transform(*this) { PushTransform(); }
	~Object() {}

	template <typename T> std::shared_ptr<Component> AddComponent()
	{
		auto component = std::dynamic_pointer_cast<Component, T>(std::make_shared<T>(*this));
		if (component) _components.push_back(component);
		return component;
	}
	template <> std::shared_ptr<Component> AddComponent<TransformComponent>() { return _components[0]; }
	template <typename T> std::shared_ptr<T> GetComponent()
	{
		std::shared_ptr<T> ptr;
		for (auto component : _components)
		{
			ptr = std::dynamic_pointer_cast<T, Component>(component);
			if (ptr) return ptr;
		}
		return ptr;
	}
	TransformComponent* Transform() { return &_transform; }
	virtual void Initialize() {}
	virtual void Update() {}

protected:

	TransformComponent _transform;
	std::vector<std::shared_ptr<Component>> _components;

	void PushTransform() { _components.push_back(std::shared_ptr<Component>(dynamic_cast<Component*>(&_transform), [](Component* ptr){})); }

};

#endif // Object_h__
