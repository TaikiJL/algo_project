/*
Author: Taiki Hagiwara
*/

#include "CameraComponent.h"

CameraComponent* CameraComponent::_mainCamera = nullptr;

CameraComponent::CameraComponent(Object& owner) : Component(owner), _nearClip(0.1f), _farClip(1000.f), _isDirty(true)
{
	Renderer::Singleton().RegisterCamera(_camera);
	Application::Singleton().RegisterResolutionChangeCallback([&](int w, int h){ this->Update(); });
	_owner.Transform()->RegisterOnCleansedCallback([&](){ this->_camera.viewMatrix = glm::inverse(_owner.Transform()->World()); });
	_mainCamera = this;
}

void CameraComponent::LoadData(const ObjectData& data)
{
	_isOrthographic = data.GetBoolVar("isOrthographic");
	_size = data.GetFloatVar("size");

	const auto& colorData = data.GetStruct("backgroundColor");
	_camera.backgroundColor = glm::vec4(colorData.GetFloatVar("r"), colorData.GetFloatVar("g"), colorData.GetFloatVar("b"), colorData.GetFloatVar("a"));

	Update();
}

void CameraComponent::Update()
{
	if (_isDirty)
	{
		if (_isOrthographic)
		{
			const Application& applicationManager = Application::Singleton();
			float x = (applicationManager.RenderWidth() * _size) / applicationManager.RenderHeight();
			_camera.projectionMatrix = glm::ortho(-x, x, -_size, _size, _nearClip, _farClip);
		}
		else
		{

		}

		_isDirty = false;
	}
}

void CameraComponent::UpdateRenderList(const std::vector<std::weak_ptr<GraphicsComponent>>& graphics)
{
	std::vector<std::weak_ptr<GraphicsComponent>> newRenderList;
	_camera.geometriesList.clear();
	for (const auto& ptr : graphics)
	{
		std::shared_ptr<GraphicsComponent> geometry(ptr);
		if (geometry->IsEnabled())
		{
			geometry->ReferenceMesh();
			newRenderList.push_back(ptr);
			_camera.geometriesList.push_back(Geometry(geometry->GetMesh(), geometry->ModelMatrix(), &(geometry->Color())));
		}
	}

	for (const auto& ptr : _previousRenderList)
	{
		std::shared_ptr<GraphicsComponent> geometry(ptr);
		geometry->UnreferenceMesh();
	}

	_previousRenderList = std::move(newRenderList);
}

glm::vec3 CameraComponent::Screen2World(int x, int y, float desiredZ) const
{
	float viewportX, viewportY;
	Window::Singleton().Screen2Viewport(x, y, &viewportX, &viewportY);
	glm::mat4 inverseMat = glm::inverse(_camera.viewMatrix) * glm::inverse(_camera.projectionMatrix);
	glm::vec4 worldPosition = inverseMat * glm::vec4(viewportX, viewportY, 0.f, 1.f);
	return glm::vec3(worldPosition.x, worldPosition.y, desiredZ);
}
