/*
Author: Taiki Hagiwara
*/

#pragma once

#include <unordered_map>
#include <string>

#include "../third_party/tinyxml2/tinyxml2.h"

typedef std::unordered_map<std::string, float>			FloatVar;
typedef std::unordered_map<std::string, int>			IntVar;
typedef std::unordered_map<std::string, std::string>	StringVar;
typedef std::unordered_map<std::string, bool>			BoolVar;

class ObjectData
{

public:

	ObjectData() {}
	ObjectData& operator=(ObjectData&& other);

	float				GetFloatVar(const std::string& name)	const { return _floats.at(name); }
	int					GetIntVar(const std::string& name)		const { return _ints.at(name); }
	const std::string&	GetStringVar(const std::string& name)	const { return _strings.at(name); }
	bool				GetBoolVar(const std::string& name)		const { return _bools.at(name); }
	const ObjectData&	GetStruct(const std::string& name)		const { return _objects.at(name); }

	void FillData(const tinyxml2::XMLElement* node);

private:

	typedef std::unordered_map<std::string, ObjectData> ObjectVar;

	ObjectVar	_objects;
	StringVar	_strings;
	FloatVar	_floats;
	IntVar		_ints;
	BoolVar		_bools;

};
