/*
Author: Taiki Hagiwara
*/

#ifndef Component_h__
#define Component_h__

#include "ObjectData.h"

class Object;

class Component
{

public:

	virtual ~Component() {}

	virtual void LoadData(const ObjectData& data) = 0;
	virtual void Update() = 0;

	bool IsEnabled() const { return _isEnabled; }
	void IsEnabled(bool enable) { _isEnabled = enable; }

protected:

	Component(Object& owner) : _owner(owner), _isEnabled(true) {}
	Object& _owner;
	bool _isEnabled;

};
#endif // Component_h__
