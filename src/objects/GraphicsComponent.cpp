/*
Author: Taiki Hagiwara
*/

#include "GraphicsComponent.h"

void GraphicsComponent::LoadData(const ObjectData& data)
{
	const auto& colorData = data.GetStruct("color");
	_color = glm::vec4(colorData.GetFloatVar("r"), colorData.GetFloatVar("g"), colorData.GetFloatVar("b"), colorData.GetFloatVar("a"));

	const std::string& meshName = data.GetStringVar("mesh");
	_mesh = &(Assets::Singleton().RetrieveMesh(meshName));
}
