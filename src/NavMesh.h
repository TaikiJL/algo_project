/*

Adrien Deban
Nicolas Bordas
Taiki Hagiwara
Jeremy Wurtzel

Projet final
8INF870 - Algorithmique

Mesh de navigation

*/

#pragma once

#include <glm/vec3.hpp>
#include <iostream>
#include <sstream>
#include <unordered_map>
#include <algorithm>
#include <cmath>

#include "objects/Object.h"
#include "objects/ObjectData.h"
#include "objects/GraphicsComponent.h"
#include "Scene.h"
#include "Obstacle.h"
#include "Terrain.h"
#include "assets/Mesh.h"
#include "managers/Input.h"


struct Point								//**D�finition d une liste de segment o� il est interdit de se d�placer:
{
	Point() :X(0.f), Y(0.f){}
	Point(float x, float y) :X(x), Y(y){}
	float X;
	float Y;
};

struct Polygone								//**D�finition d un polygone:
{
	std::vector<Point> Vertices;
	std::vector<Point> Milieu;
};

struct Segment								//**D�finition d un segment:
{
	Point A;
	Point B;
};


class NavMesh : public Object
{

public:

	struct Noeud
	{
		glm::vec3 position;
		std::vector<unsigned int> noeudsAdj;
	};
	struct Polygon
	{
		std::vector<unsigned int> vertices;
		std::vector<unsigned int> noeuds;
	};

	static std::shared_ptr<Object> CreationNavMesh(const ObjectData& donnees);

	NavMesh() {}
	~NavMesh() {}

	void Initialize() override;
	void Update() override {}
	const std::vector<Polygon>& Polygons() const { return _polygons; }
	const std::vector<Noeud>& Noeuds() const { return _noeuds; }
	const std::vector<glm::vec3>& Vertices() const { return *_vertices; }

private:

	Mesh _navMesh;
	std::vector<Polygon> _polygons;
	std::vector<Noeud> _noeuds;
	const std::vector<glm::vec3>* _vertices;

	void GenererMesh(const std::vector<const Mesh*>& obstacles, const Mesh* terrain);
	void GenererMesh(const char* chemin, float scale = 0.01f);


	/* Fonctions pour la recup�ration du contour des obstacles */

	void AfficherPointObstacles(std::vector<Point> listePoint);
	void AfficherUnPoint(Point unPoint);
	void AfficherMatriceObstacles(std::vector < std::vector < Point> > matPoint);
	std::vector<std::vector<Point>> CopierPointObstacles(const std::vector<const Mesh*>& obstacles);
	std::vector < std::vector < Point> > TrouverObstacles(const std::vector<const Mesh*>& obstacles, const Mesh* terrain);
	Point PlusGrandX(std::vector<Point> listePoint);
	std::vector<Point> TrouverPolygoneDePoint(std::vector<Point> listePoint, Point unPoint);
	Point trouverSuivantNoeudObstacle(Point unPoint, Point deuxPoint, std::vector<Point> listePoint);

	/* Fonctions pour la triangulation */
	std::vector<Polygone> FaireNavMesh(std::vector < std::vector < Point> > matPointObstacles, const Mesh* terrain);
	std::vector < Point> TrouverPointTerrain(const Mesh* terrain);
	bool Intersection(float x1, float y1, float x2, float y2, float x3, float y3, float x4, float y4);
	bool EstDansPolygone(Polygone polygone, float x, float y);
	bool IntersectionSegmentPolygone(Polygone polygone, Point A, Point B);
	std::vector<Polygone> Trianguler(std::vector<Polygone> listePolygoneOrigine, std::vector<Polygone> listePolygoneNouveau,Segment segment, int p);
	void AfficherSegment(Point A, Point B, Point C, Point D);
	Point RecupPointIntersectionSegment(float x1, float y1, float x2, float y2, float x3, float y3, float x4, float y4);
	bool PointIntersectionSegmentDroite(float dx1, float dy1, float dx2, float dy2, float sx3, float sy3, float sx4, float sy4);
	Point RecupPointIntersectionSegmentDroite(float dx1, float dy1, float dx2, float dy2, float sx3, float sy3, float sx4, float sy4);
	bool EstEntreSegment(Point A, Point B, Point C);
	std::vector < Polygone> CreerDeuxTriangles(int positionPolynome, Point A, std::vector<Polygone> listepolygone, Segment segment);
	bool PointEstEgal(Point A, Point B);
};
