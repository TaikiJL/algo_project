/*

Adrien Deban
Nicolas Bordas
Taiki Hagiwara
Jeremy Wurtzel

Projet final
8INF870 - Algorithmique

Mesh de navigation

*/

#ifndef Agent_h__
#define Agent_h__

#include <functional>
#include <glm/gtx/compatibility.hpp>

#include "objects/Object.h"
#include "objects/ObjectData.h"
#include "objects/CameraComponent.h"
#include "managers/Input.h"
#include "managers/Time.h"
#include "AStar.h"
#include "Scene.h"
#include "NavMesh.h"

class Agent : public Object
{

public:

	static std::shared_ptr<Object> CreationAgent(const ObjectData& donnees);

	Agent() : _moveSpeed(1.f), _isTargetAssigned(false), _currentNodeIndex(0) {}
	~Agent() {}

	void Initialize() override;
	void Update() override;

	void MoveTowards(const glm::vec3& targetPos, float speed);
	void MoveTo(const glm::vec3& targetPos, float speed);
	bool IsTargetAssigned() const { return _isTargetAssigned; }

private:

	glm::vec3 _targetPos;
	glm::vec3 _targetDirection;
	std::vector<glm::vec3> _pathNodes;
	int _currentNodeIndex;
	float _moveSpeed;
	bool _isTargetAssigned;

	std::vector<glm::vec3> BezierSmoothing(const std::vector<glm::vec3>& path, int globalSteps = 5);

};

#endif // Agent_h__
