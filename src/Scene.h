/*

Adrien Deban
Nicolas Bordas
Taiki Hagiwara
Jeremy Wurtzel

Projet final
8INF870 - Algorithmique

Mesh de navigation

*/

#ifndef Scene_h__
#define Scene_h__

#include <vector>
#include <memory>
#include <map>
#include <unordered_map>
#include <string>
#include <functional>
#include <iostream>

#include "third_party/tinyxml2/tinyxml2.h"
#include "managers/Assets.h"
#include "assets/Mesh.h"
#include "objects/ObjectData.h"
#include "objects/Object.h"
#include "objects/TransformComponent.h"
#include "objects/GraphicsComponent.h"
#include "objects/CameraComponent.h"

typedef std::unordered_map<std::string, std::function<std::shared_ptr<Object>(const ObjectData& data)>> ObjectCreationMap;

class Scene
{

public:

	Scene(const char* filePath);
	~Scene() {}

	static Scene& Current() { return *_current; }
	static void SetObjectCreationMap(const ObjectCreationMap& map) { _objectCreators = &map; }

	void Initialize();
	void Update();
	// Return the object bearing the name.
	std::shared_ptr<Object> GetObject(const std::string& name) { return _objects.at(name); }
	// Returns an array of objects of type T in the scene.
	template <typename T> std::vector<std::shared_ptr<T>> GetObjectsOfType()
	{
		std::vector<std::shared_ptr<T>> objects;
		for (auto object : _objects)
		{
			auto ptr = std::dynamic_pointer_cast<T, Object>(object.second);
			if (ptr) objects.push_back(ptr);
		}
		return (objects);
	}
	// Returns the first object of type T found in the scene.
	template <typename T> std::shared_ptr<T> GetObjectOfType()
	{
		std::shared_ptr<T> ptr;
		for (auto object : _objects)
		{
			ptr = std::dynamic_pointer_cast<T, Object>(object.second);
			if (ptr) return ptr;
		}
		return ptr;
	}
	void AddObject(const std::string& name, std::shared_ptr<Object> object);
	void AddComponentToCollection(std::shared_ptr<GraphicsComponent> graphics) { _graphics.push_back(std::weak_ptr<GraphicsComponent>(graphics)); }

private:

	enum class ComponentType
	{
		TRANSFORM, GRAPHICS, CAMERA
	};

	static Scene* _current;
	static const ObjectCreationMap* _objectCreators;
	static const std::unordered_map<std::string, ComponentType> COMPONENT_TYPES;

	std::map<std::string, std::shared_ptr<Object>> _objects;
	std::vector<std::weak_ptr<TransformComponent>> _transforms;
	std::vector<std::weak_ptr<GraphicsComponent>> _graphics;
	std::vector<std::weak_ptr<CameraComponent>> _cameras;

	void LoadSettings(const tinyxml2::XMLElement* const sceneRootNode);
	void LoadAssets(const tinyxml2::XMLElement* const sceneRootNode);
	void LoadObjects(const tinyxml2::XMLElement* const sceneRootNode);

};

#endif // Scene_h__
