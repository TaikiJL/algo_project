/*

Adrien Deban
Nicolas Bordas
Taiki Hagiwara
Jeremy Wurtzel

Projet final
8INF870 - Algorithmique

Mesh de navigation

*/

#pragma once

#include "objects/Object.h"
#include "objects/ObjectData.h"

class Obstacle : public Object
{

public:

	static std::shared_ptr<Object> CreationObstacle(const ObjectData& donnees);

	Obstacle() {}
	~Obstacle() {}

	void Initialize() override {}
	void Update() override {}

private:



};
