/*

Adrien Deban
Nicolas Bordas
Taiki Hagiwara
Jeremy Wurtzel

Projet final
8INF870 - Algorithmique

Mesh de navigation

*/

#include "managers/Application.h"
#include "managers/Window.h"
#include "managers/Input.h"
#include "managers/Assets.h"
#include "managers/Renderer.h"
#include "managers/Time.h"
#include "Scene.h"
#include "Obstacle.h"
#include "Terrain.h"
#include "NavMesh.h"
#include "Agent.h"

Application	applicationManager;
Window		window;
Input		inputManager;
Assets		assetManager;
Renderer	renderManager;
Time		timeManager;

int main(int argc, char* argv[])
{
	int errorCode = 0;

	applicationManager.SetResolution(800, 600);

	applicationManager.Initialize();

	timeManager.Initialize();

	errorCode = window.Initialize();

	if (errorCode != 0)
		return errorCode;

	inputManager.Initialize();

	errorCode = renderManager.Initialize();

	if (errorCode != 0)
	{
		window.ShutDown();
		return errorCode;
	}

	ObjectCreationMap objCreationMap = {
		{ "obstacle", &Obstacle::CreationObstacle },
		{ "terrain", &Terrain::CreationTerrain },
		{ "navmesh", &NavMesh::CreationNavMesh },
		{ "agent", &Agent::CreationAgent },
	};

	Scene::SetObjectCreationMap(objCreationMap);

	Scene scene("data/scene.xml");

	// main loop

	scene.Initialize();

	while (applicationManager.IsRunning())
	{
		timeManager.UpdateDeltaTime();
		inputManager.Update();
		scene.Update();
		renderManager.Render();
		window.Swapbuffers();
	}

	//errorCode = renderManager.ShutDown();

	if (errorCode != 0)
		return errorCode;

	inputManager.ShutDown();

	errorCode = window.ShutDown();

	if (errorCode != 0)
		return errorCode;

	applicationManager.ShutDown();

	return 0;
}
