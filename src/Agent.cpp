/*

Adrien Deban
Nicolas Bordas
Taiki Hagiwara
Jeremy Wurtzel

Projet final
8INF870 - Algorithmique

Mesh de navigation

*/

#include "Agent.h"

#define SQUARE(x) x * x

std::shared_ptr<Object> Agent::CreationAgent(const ObjectData& donnees)
{
	auto agent = std::make_shared<Agent>();
	auto ptr = std::dynamic_pointer_cast<Object, Agent>(agent);

	return ptr;
}

void Agent::Initialize()
{
	Input::Singleton().RegisterMouseCommand(SDL_BUTTON_LEFT, SDL_MOUSEBUTTONDOWN, [&](){
		const Camera& camera = Renderer::Singleton().GetCamera();
		int mouseX, mouseY;
		SDL_GetMouseState(&mouseX, &mouseY);
		this->MoveTo(CameraComponent::Main().Screen2World(mouseX, mouseY, 0.f), 2.5f);
	});
}

void Agent::Update()
{
	if (!_isTargetAssigned) return;

	_transform.Translate(_targetDirection * Time::DeltaTime() * _moveSpeed);

	const glm::vec3& currentPos = _transform.Position();
	float dx = _targetPos.x - currentPos.x;
	float dy = _targetPos.y - currentPos.y;
	if (dx * dx + dy * dy <= SQUARE(0.05f))
	{
		_isTargetAssigned = false;
		if (_currentNodeIndex > 0)
		{
			MoveTowards(_pathNodes[--_currentNodeIndex], _moveSpeed);
		}
	}
}

void Agent::MoveTowards(const glm::vec3& targetPos, float speed)
{
	_isTargetAssigned = true;
	_moveSpeed = speed;

	_targetPos = targetPos;
	_targetDirection = glm::normalize(targetPos - _transform.Position());
}

void Agent::MoveTo(const glm::vec3& targetPos, float speed)
{
	auto navMesh = Scene::Current().GetObjectOfType<NavMesh>();
	_pathNodes = Astar(*navMesh, _transform.Position(), targetPos);
	
	if (_pathNodes.size() > 0)
	{
		//_pathNodes = BezierSmoothing(_pathNodes);
		if (_pathNodes.size() > 1) _pathNodes.pop_back();
		_currentNodeIndex = _pathNodes.size() - 1;
		MoveTowards(_pathNodes[_currentNodeIndex], speed);
	}
}

std::vector<glm::vec3> Agent::BezierSmoothing(const std::vector<glm::vec3>& path, int globalSteps)
{
	std::vector<std::vector<glm::vec3>> tab(path.size(), path);
	std::vector<glm::vec3> points;
	int pas = globalSteps * path.size() - 1;

	for (int i = 0; i < pas; ++i)
	{
		glm::vec3 t;
		for (int j = 1; j < tab[0].size(); ++j)
		{
			for (int k = 0; k < tab[0].size() - k; ++k)
			{
				// trouve le point au ratio t de chaque sergment du polygone de controle
				t = tab[j - 1][k] + ((tab[j - 1][k + 1] - tab[j - 1][k]) * (float)i / (float)pas);
				tab[j][k] = t;
			}
		}
		points.push_back(tab.back()[0]);
	}

	return points;
}
